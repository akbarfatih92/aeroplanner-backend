package fontys.s3.aeroplannerbackend.domains.aircraft;

import fontys.s3.aeroplannerbackend.domains.aircraft.Aircraft;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GetAllAircraftResponse {
    private List<Aircraft> aircrafts;
}
