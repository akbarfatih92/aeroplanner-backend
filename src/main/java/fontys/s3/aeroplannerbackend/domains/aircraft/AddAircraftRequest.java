package fontys.s3.aeroplannerbackend.domains.aircraft;

import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AddAircraftRequest {
    @NotBlank
    @Length(max = 5)
    private String code;

    @NotBlank
    @Length(max = 50)
    private String name;
}
