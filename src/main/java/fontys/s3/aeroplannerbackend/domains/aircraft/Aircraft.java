package fontys.s3.aeroplannerbackend.domains.aircraft;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Aircraft {
    private Long id;
    private String code;
    private String name;
}
