package fontys.s3.aeroplannerbackend.domains.flightplan;

import jakarta.validation.constraints.Min;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GetAllFlightPlanRequest {
    @Min(1)
    private Integer status;
    @Min(1)
    private Long userId;
}
