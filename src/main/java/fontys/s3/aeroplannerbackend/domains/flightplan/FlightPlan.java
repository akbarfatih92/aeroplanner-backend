package fontys.s3.aeroplannerbackend.domains.flightplan;

import fontys.s3.aeroplannerbackend.domains.flighttype.FlightType;
import fontys.s3.aeroplannerbackend.domains.aircraft.Aircraft;
import fontys.s3.aeroplannerbackend.domains.airport.Airport;
import fontys.s3.aeroplannerbackend.domains.user.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FlightPlan {


    private User user;
    private Long number;
    private Integer status;
    private Airport origin;
    private Airport dest;
    private LocalDateTime originDateTime;
    private LocalDateTime destDateTime;
    private Aircraft aircraft;
    private FlightType flightType;
    private Integer flightLevel;
    private Integer cruiseSpeed;
    private Integer flightNumber;
    private Integer flightRule;
    private String note;
    private Long duration;
}
