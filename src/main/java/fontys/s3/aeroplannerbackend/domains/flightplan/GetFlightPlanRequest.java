package fontys.s3.aeroplannerbackend.domains.flightplan;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GetFlightPlanRequest {
    @NotNull
    @Min(1)
    private Long userId;

    @NotNull
    @Min(1)
    private Long number;
}
