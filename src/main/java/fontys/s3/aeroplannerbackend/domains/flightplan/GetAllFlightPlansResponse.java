package fontys.s3.aeroplannerbackend.domains.flightplan;

import fontys.s3.aeroplannerbackend.domains.flightplan.FlightPlan;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GetAllFlightPlansResponse {
    private List<FlightPlan> flightPlans;
}
