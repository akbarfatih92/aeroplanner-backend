package fontys.s3.aeroplannerbackend.domains.flightplan;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreateFlightPlanResponse {
    private Long userId;
    private Long number;
}
