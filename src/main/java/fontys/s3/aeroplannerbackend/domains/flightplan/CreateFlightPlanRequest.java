package fontys.s3.aeroplannerbackend.domains.flightplan;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreateFlightPlanRequest {
    @NotNull
    @Min(0)
    private Long userId;

    @NotNull
    @Min(0)
    @Max(6)
    private Integer status;

    @NotNull
    private Long originId;

    @NotNull
    private Long destId;

    @NotNull
    private LocalDateTime originDateTime;

    @NotNull
    private LocalDateTime destDateTime;

    @NotNull
    private Long aircraftId;

    @NotNull
    private Long flightTypeId;

    @NotNull
    @Min(0)
    @Max(1000)
    private Integer flightLevel;

    @NotNull
    @Min(0)
    @Max(10000)
    private Integer cruiseSpeed;

    @NotNull
    @Min(0)
    @Max(1)
    private Integer flightRule;

    @Min(0)
    private Integer flightNumber;

    @Length(max = 100)
    private String note;
}
