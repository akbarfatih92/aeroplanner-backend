package fontys.s3.aeroplannerbackend.domains.airport;

import fontys.s3.aeroplannerbackend.domains.airport.Airport;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GetAllAirportResponse {
    private List<Airport> airports;
}
