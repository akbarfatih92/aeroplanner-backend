package fontys.s3.aeroplannerbackend.domains.flighttype;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FlightType {
    private Long id;
    private String title;
}
