package fontys.s3.aeroplannerbackend.domains.flighttype;

import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AddFlightTypeRequest {
    @NotBlank
    @Length(max = 50)
    private String title;
}
