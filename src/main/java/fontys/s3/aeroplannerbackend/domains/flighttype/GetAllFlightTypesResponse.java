package fontys.s3.aeroplannerbackend.domains.flighttype;

import fontys.s3.aeroplannerbackend.domains.flighttype.FlightType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GetAllFlightTypesResponse {
    private List<FlightType> flightTypes;
}
