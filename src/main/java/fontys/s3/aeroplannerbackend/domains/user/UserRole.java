package fontys.s3.aeroplannerbackend.domains.user;

public enum UserRole {
    PILOT,
    ATC,
    ADMIN
}
