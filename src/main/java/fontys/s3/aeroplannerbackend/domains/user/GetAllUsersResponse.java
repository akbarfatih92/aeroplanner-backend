package fontys.s3.aeroplannerbackend.domains.user;

import fontys.s3.aeroplannerbackend.domains.user.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GetAllUsersResponse {
    private List<User> users;
}
