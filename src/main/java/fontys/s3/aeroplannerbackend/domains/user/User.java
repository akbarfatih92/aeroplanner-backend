package fontys.s3.aeroplannerbackend.domains.user;

import lombok.*;
import java.sql.Timestamp;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class User {
    private Long id;
    private String username;
    private UserRole role;
    private Timestamp createTime;
}
