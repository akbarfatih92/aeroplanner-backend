package fontys.s3.aeroplannerbackend.controller;

import fontys.s3.aeroplannerbackend.business.airport.AddAirportUseCase;
import fontys.s3.aeroplannerbackend.business.airport.DeleteAirportUseCase;
import fontys.s3.aeroplannerbackend.business.airport.GetAllAirportsUseCase;
import fontys.s3.aeroplannerbackend.domains.GenericIdRequest;
import fontys.s3.aeroplannerbackend.domains.GenericIdResponse;
import fontys.s3.aeroplannerbackend.domains.airport.AddAirportRequest;
import fontys.s3.aeroplannerbackend.domains.airport.GetAllAirportResponse;
import jakarta.annotation.security.RolesAllowed;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/airports")
@AllArgsConstructor
@CrossOrigin(origins = {"http://localhost:5173","http://localhost:8080"})
public class AirportController {
    private final GetAllAirportsUseCase getAllAirportsUseCase;
    private final AddAirportUseCase addAirportUseCase;
    private final DeleteAirportUseCase deleteAirportUseCase;

    @GetMapping()
    @RolesAllowed({"PILOT","ATC","ADMIN"})
    public ResponseEntity<GetAllAirportResponse> getAllAirports(){
        GetAllAirportResponse response = getAllAirportsUseCase.getAllAirports();
        return ResponseEntity.ok(response);
    }

    @PostMapping()
    public ResponseEntity<GenericIdResponse> addAirport(@RequestBody @Valid AddAirportRequest request){
        GenericIdResponse response = addAirportUseCase.addAirport(request);
        return ResponseEntity.status(HttpStatus.CREATED).body(response);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteAirport(@PathVariable("id") long id){
        GenericIdRequest request = GenericIdRequest.builder().id(id).build();
        deleteAirportUseCase.deleteAirport(request);
        return ResponseEntity.ok("Deleted Successfully");
    }
}
