package fontys.s3.aeroplannerbackend.controller;

import fontys.s3.aeroplannerbackend.business.flightplan.CreateFlightPlanUseCase;
import fontys.s3.aeroplannerbackend.business.flightplan.DeleteFlightPlanUseCase;
import fontys.s3.aeroplannerbackend.business.flightplan.GetAllFlightPlansUseCase;

import fontys.s3.aeroplannerbackend.business.flightplan.GetFlightPlanUseCase;
import fontys.s3.aeroplannerbackend.domains.flightplan.FlightPlan;
import fontys.s3.aeroplannerbackend.domains.flightplan.CreateFlightPlanRequest;
import fontys.s3.aeroplannerbackend.domains.flightplan.DeleteFlightPlanRequest;
import fontys.s3.aeroplannerbackend.domains.flightplan.GetAllFlightPlanRequest;
import fontys.s3.aeroplannerbackend.domains.flightplan.GetFlightPlanRequest;
import fontys.s3.aeroplannerbackend.domains.flightplan.CreateFlightPlanResponse;
import fontys.s3.aeroplannerbackend.domains.flightplan.GetAllFlightPlansResponse;
import jakarta.annotation.security.RolesAllowed;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/flightplans")
@AllArgsConstructor
@CrossOrigin(origins = {"http://localhost:5173","http://localhost:8080"})
public class FlightPlanController {
    private final GetAllFlightPlansUseCase getAllFlightPlansUseCase;
    private final GetFlightPlanUseCase getFlightPlanUseCase;
    private final CreateFlightPlanUseCase createFlightPlanUseCase;
    private final DeleteFlightPlanUseCase deleteFlightPlanUseCase;
    @GetMapping()
    @RolesAllowed({"ATC","ADMIN"})
    public ResponseEntity<GetAllFlightPlansResponse> getAllFlightPlans(
            @RequestParam(value = "userId", required = false) Long userId,
            @RequestParam(value = "status", required = false) Integer status
    ){
        GetAllFlightPlanRequest request = GetAllFlightPlanRequest.builder().userId(userId).status(status).build();
        GetAllFlightPlansResponse response = getAllFlightPlansUseCase.getAllFlightPlans(request);
        return ResponseEntity.ok(response);
    }


    @GetMapping("/{userId}/{number}")
    @RolesAllowed({"PILOT","ATC","ADMIN"})
    public ResponseEntity<FlightPlan> getFlightPlan(
            @PathVariable("userId") long userId
            ,@PathVariable("number") long number
    ){
        GetFlightPlanRequest request = GetFlightPlanRequest.builder().userId(userId).number(number).build();
        final Optional<FlightPlan> optionalFlightPlan = getFlightPlanUseCase.getFlightPlan(request);
        return optionalFlightPlan.map(flightPlan -> ResponseEntity.ok().body(flightPlan)).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PostMapping()
    @RolesAllowed({"PILOT"})
    public ResponseEntity<CreateFlightPlanResponse> createFlightPlan(@RequestBody @Valid CreateFlightPlanRequest request){
        CreateFlightPlanResponse response = createFlightPlanUseCase.createFlightPlan(request);
        return ResponseEntity.status(HttpStatus.CREATED).body(response);
    }

    @DeleteMapping("/{userId}/{number}")
    @RolesAllowed({"PILOT","ADMIN"})
    public ResponseEntity<String> deleteFlightPlan(
            @PathVariable("userId") long userId
            ,@PathVariable("number") long number
    ){
        DeleteFlightPlanRequest request = DeleteFlightPlanRequest.builder().userId(userId).number(number).build();
        deleteFlightPlanUseCase.deleteFlightPlan(request);
        return ResponseEntity.ok("Deleted Succesfully");
    }
}
