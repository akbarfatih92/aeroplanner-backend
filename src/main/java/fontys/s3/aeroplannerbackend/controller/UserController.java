package fontys.s3.aeroplannerbackend.controller;

import fontys.s3.aeroplannerbackend.business.user.CreateUserUseCase;
import fontys.s3.aeroplannerbackend.business.user.DeleteUserUseCase;
import fontys.s3.aeroplannerbackend.business.user.GetAllUsersUseCase;
import fontys.s3.aeroplannerbackend.business.user.GetUserUseCase;
import fontys.s3.aeroplannerbackend.domains.GenericIdRequest;
import fontys.s3.aeroplannerbackend.domains.GenericIdResponse;
import fontys.s3.aeroplannerbackend.domains.user.CreateUserRequest;
import fontys.s3.aeroplannerbackend.domains.user.GetAllUsersResponse;
import fontys.s3.aeroplannerbackend.domains.user.User;
import jakarta.annotation.security.RolesAllowed;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/users")
@AllArgsConstructor
@CrossOrigin(origins = {"http://localhost:5173","http://localhost:8080"})
public class UserController {
    private final GetAllUsersUseCase getAllUsersUseCase;
    private final GetUserUseCase getUserUseCase;
    private final CreateUserUseCase createUserUseCase;
    private final DeleteUserUseCase deleteUserUseCase;

    @GetMapping()
    @RolesAllowed({"ADMIN"})
    public ResponseEntity<GetAllUsersResponse> getAllUsers(){
        GetAllUsersResponse response = getAllUsersUseCase.getAllUsers();
        return ResponseEntity.ok(response);
    }

    @GetMapping("/{userId}")
    @RolesAllowed({"PILOT","ATC","ADMIN"})
    public ResponseEntity<User> getUser(@PathVariable("userId") long userId){
        GenericIdRequest request = GenericIdRequest.builder().id(userId).build();
        final Optional<User> optionalUser = getUserUseCase.getUser(request);
        return optionalUser.map(user -> ResponseEntity.ok().body(user)).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PostMapping()
    public ResponseEntity<GenericIdResponse> createUser(@RequestBody @Valid CreateUserRequest request){
        GenericIdResponse response = createUserUseCase.createUser(request);
        return ResponseEntity.status(HttpStatus.CREATED).body(response);
    }

    @DeleteMapping("/{userId}")
    @RolesAllowed({"ADMIN"})
    public ResponseEntity<String> deleteUser(@PathVariable("userId") long userId){
        GenericIdRequest request = GenericIdRequest.builder().id(userId).build();
        deleteUserUseCase.deleteUser(request);
        return ResponseEntity.ok("Deleted Succesfully");
    }
}
