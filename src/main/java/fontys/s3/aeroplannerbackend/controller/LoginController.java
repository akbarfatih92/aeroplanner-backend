package fontys.s3.aeroplannerbackend.controller;

import fontys.s3.aeroplannerbackend.business.login.LoginUseCase;
import fontys.s3.aeroplannerbackend.domains.login.LoginRequest;
import fontys.s3.aeroplannerbackend.domains.login.LoginResponse;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/login")
@AllArgsConstructor
@CrossOrigin(origins = {"http://localhost:5173","http://localhost:8080"})
public class LoginController {
    private final LoginUseCase loginUseCase;

    @PostMapping()
    public ResponseEntity<LoginResponse> login(@RequestBody @Valid LoginRequest request){
        LoginResponse response = loginUseCase.login(request);
        return ResponseEntity.ok().body(response);
    }
}
