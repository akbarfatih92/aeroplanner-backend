package fontys.s3.aeroplannerbackend.controller;

import fontys.s3.aeroplannerbackend.business.flighttype.AddFlightTypeUseCase;
import fontys.s3.aeroplannerbackend.business.flighttype.DeleteFlightTypeUseCase;
import fontys.s3.aeroplannerbackend.business.flighttype.GetAllFlightTypesUseCase;
import fontys.s3.aeroplannerbackend.domains.GenericIdRequest;
import fontys.s3.aeroplannerbackend.domains.GenericIdResponse;
import fontys.s3.aeroplannerbackend.domains.flighttype.AddFlightTypeRequest;
import fontys.s3.aeroplannerbackend.domains.flighttype.GetAllFlightTypesResponse;
import jakarta.annotation.security.RolesAllowed;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/flighttypes")
@AllArgsConstructor
@CrossOrigin(origins = {"http://localhost:5173","http://localhost:8080"})
public class FlightTypeController {
    private final GetAllFlightTypesUseCase getAllFlightTypesUseCase;
    private final AddFlightTypeUseCase addFlightTypeUseCase;
    private final DeleteFlightTypeUseCase deleteFlightTypeUseCase;

    @GetMapping()
    @RolesAllowed({"PILOT","ATC","ADMIN"})
    public ResponseEntity<GetAllFlightTypesResponse> getAllFlightTypes(){
        GetAllFlightTypesResponse response = getAllFlightTypesUseCase.getAllFlightTypes();
        return ResponseEntity.ok(response);
    }

    @PostMapping
    public ResponseEntity<GenericIdResponse> addFlightType(@RequestBody @Valid AddFlightTypeRequest request){
        GenericIdResponse response = addFlightTypeUseCase.addFlightType(request);
        return ResponseEntity.status(HttpStatus.CREATED).body(response);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteFlightType(@PathVariable("id") long id){
        GenericIdRequest request = GenericIdRequest.builder().id(id).build();
        deleteFlightTypeUseCase.deleteFlightType(request);
        return ResponseEntity.ok("Deleted Successfully");
    }
}
