package fontys.s3.aeroplannerbackend.controller;

import fontys.s3.aeroplannerbackend.business.aircraft.AddAircraftUseCase;
import fontys.s3.aeroplannerbackend.business.aircraft.DeleteAircraftUseCase;
import fontys.s3.aeroplannerbackend.business.aircraft.GetAllAircraftsUseCase;
import fontys.s3.aeroplannerbackend.domains.GenericIdRequest;
import fontys.s3.aeroplannerbackend.domains.GenericIdResponse;
import fontys.s3.aeroplannerbackend.domains.aircraft.AddAircraftRequest;
import fontys.s3.aeroplannerbackend.domains.aircraft.GetAllAircraftResponse;
import jakarta.annotation.security.RolesAllowed;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/aircrafts")
@AllArgsConstructor
@CrossOrigin(origins = {"http://localhost:5173","http://localhost:8080"})
public class AircraftController {
    private final GetAllAircraftsUseCase getAllAircraftsUseCase;
    private final AddAircraftUseCase addAircraftUseCase;
    private final DeleteAircraftUseCase deleteAircraftUseCase;

    @GetMapping()
    @RolesAllowed({"PILOT","ATC","ADMIN"})
    public ResponseEntity<GetAllAircraftResponse> getAllAircrafts(){
        GetAllAircraftResponse response = getAllAircraftsUseCase.getAllAircrafts();
        return ResponseEntity.ok(response);
    }

    @PostMapping()
    public ResponseEntity<GenericIdResponse> addAircraft(@RequestBody @Valid AddAircraftRequest request){
        GenericIdResponse response = addAircraftUseCase.addAircraft(request);
        return ResponseEntity.status(HttpStatus.CREATED).body(response);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteAircraft(@PathVariable("id") long id){
        GenericIdRequest request = GenericIdRequest.builder().id(id).build();
        deleteAircraftUseCase.deleteAircraft(request);
        return ResponseEntity.ok("Deleted Successfully");
    }
}
