package fontys.s3.aeroplannerbackend.persistence;

import fontys.s3.aeroplannerbackend.persistence.entity.AircraftEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AircraftRepo extends JpaRepository<AircraftEntity,Long> {}
