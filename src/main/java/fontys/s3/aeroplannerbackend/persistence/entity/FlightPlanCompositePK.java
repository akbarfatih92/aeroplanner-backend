package fontys.s3.aeroplannerbackend.persistence.entity;

import jakarta.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Embeddable
public class FlightPlanCompositePK implements Serializable {
    private UserEntity user;
    private Long number;
}
