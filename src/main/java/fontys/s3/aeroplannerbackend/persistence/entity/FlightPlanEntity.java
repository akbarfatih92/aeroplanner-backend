package fontys.s3.aeroplannerbackend.persistence.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.*;
import org.hibernate.validator.constraints.Length;

import java.time.LocalDateTime;

@Entity
@IdClass(FlightPlanCompositePK.class)
@Table(name = "flightplan")
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FlightPlanEntity {
    @Id
    @NotNull
    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserEntity user;

    @Id
    @NotNull
    @Column(name = "number")
    private Long number;

    @Column(name = "status")
    @NotNull
    @Min(0)
    @Max(6)
    private Integer status;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "origin_id")
    private AirportEntity origin;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "dest_id")
    private AirportEntity dest;

    @NotNull
    @Column(name = "origin_datetime")
    private LocalDateTime originDateTime;

    @NotNull
    @Column(name = "dest_datetime")
    private LocalDateTime destDateTime;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "aircraft_id")
    private AircraftEntity aircraft;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "flighttype_id")
    private FlightTypeEntity flightType;

    @Column(name = "flight_level")
    @NotNull
    @Min(0)
    @Max(1000)
    private Integer flightLevel;

    @Column(name = "cruise_speed")
    @NotNull
    @Min(0)
    @Max(10000)
    private Integer cruiseSpeed;

    @Column(name = "flight_rule")
    @NotNull
    @Min(0)
    @Max(1)
    private Integer flightRule;


    @Column(name = "flight_number")
    @Min(0)
    private Integer flightNumber;

    @Length(max = 100)
    @Column(name = "note")
    @EqualsAndHashCode.Exclude
    private String note;
}
