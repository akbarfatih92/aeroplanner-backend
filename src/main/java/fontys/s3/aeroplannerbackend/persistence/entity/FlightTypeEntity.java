package fontys.s3.aeroplannerbackend.persistence.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.*;
import org.hibernate.validator.constraints.Length;

@Entity
@Table(name = "flighttype")
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FlightTypeEntity {
    @Id
    @NotNull
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotBlank
    @Length(max = 50)
    @Column(name = "title")
    @EqualsAndHashCode.Exclude
    private String title;
}
