package fontys.s3.aeroplannerbackend.persistence.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.*;
import org.hibernate.validator.constraints.Length;

import java.io.Serializable;
import java.sql.Timestamp;
import java.time.LocalDateTime;

@Entity
@Table(name = "user")
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotBlank
    @Length(min = 2, max = 20)
    @Column(name = "username")
    private String username;

    @NotBlank
    @Column(name = "email")
    @Length(max = 60)
    private String email;

    @NotBlank
    @Column(name = "hash")
    private String hash;

    @NotBlank
    @Column(name = "salt")
    private String salt;

    @Column(name = "role")
    @NotNull
    @Min(0)
    @Max(2)
    private Integer role;

    @Lob @Basic(fetch = FetchType.LAZY)
    @Column(name = "profile_picture", columnDefinition = "MEDIUMBLOB")
    @EqualsAndHashCode.Exclude
    private byte[] profilePicture;

    @Column(name = "create_time")
    @EqualsAndHashCode.Exclude
    private Timestamp createTime;

    @PrePersist
    protected void onCreate(){
        createTime = Timestamp.valueOf(LocalDateTime.now());
    }
}