package fontys.s3.aeroplannerbackend.persistence.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

@Entity
@Table(name = "airport")
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AirportEntity {
    @Id
    @NotNull
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotBlank
    @Length(max = 5)
    @Column(name = "code")
    private String code;

    @NotBlank
    @Length(max = 50)
    @Column(name = "name")
    private String name;
}
