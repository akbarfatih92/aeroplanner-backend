package fontys.s3.aeroplannerbackend.persistence;

import fontys.s3.aeroplannerbackend.persistence.entity.FlightPlanEntity;
import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface FlightPlanRepo extends JpaRepository<FlightPlanEntity,Long> {
    @Query("select f from FlightPlanEntity f where f.user.id = :userId")
    List<FlightPlanEntity> findAllByUserId(@Param("userId") Long userId);

    @Query("select f from FlightPlanEntity f where f.user.id = :userId and f.number = :number")
    Optional<FlightPlanEntity> findByUserIdAndNumber(@Param("userId") Long userId, @Param("number") Long number);

    @Modifying
    @Transactional
    @Query(value = "DELETE FROM flightplan WHERE user_id = :userId and number = :number", nativeQuery = true)
    void deleteByUserIdAndNumber(@Param("userId") Long userId, @Param("number") Long number);

    @Query("select count(f) from FlightPlanEntity f where f.user.id = :userId")
    Long countByUserId(@Param("userId") Long userId);

    @Transactional
    @Query("select max(f.number) from FlightPlanEntity f where f.user.id = :userId")
    Optional<Long> getNextNumber(@Param("userId") Long userId);
}
