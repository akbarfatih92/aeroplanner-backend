package fontys.s3.aeroplannerbackend.persistence;

import fontys.s3.aeroplannerbackend.persistence.entity.AirportEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AirportRepo extends JpaRepository<AirportEntity,Long> {}
