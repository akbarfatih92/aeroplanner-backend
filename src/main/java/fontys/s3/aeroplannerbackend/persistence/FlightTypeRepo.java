package fontys.s3.aeroplannerbackend.persistence;

import fontys.s3.aeroplannerbackend.persistence.entity.FlightTypeEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FlightTypeRepo extends JpaRepository<FlightTypeEntity,Long> { }
