package fontys.s3.aeroplannerbackend.business.flighttype.impl;

import fontys.s3.aeroplannerbackend.business.flighttype.DeleteFlightTypeUseCase;
import fontys.s3.aeroplannerbackend.business.flighttype.exception.InvalidFlightTypeException;
import fontys.s3.aeroplannerbackend.domains.GenericIdRequest;
import fontys.s3.aeroplannerbackend.persistence.FlightTypeRepo;
import fontys.s3.aeroplannerbackend.persistence.entity.FlightTypeEntity;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
public class DeleteFlightTypeUseCaseImpl implements DeleteFlightTypeUseCase {
    private FlightTypeRepo flightTypeRepo;
    @Override
    public void deleteFlightType(GenericIdRequest request){
        Optional<FlightTypeEntity> selectedFlightType = flightTypeRepo.findById(request.getId());

        if(selectedFlightType.isEmpty()){
            throw new InvalidFlightTypeException("INVALID_FLIGHT_TYPE_ID");
        }

        flightTypeRepo.delete(selectedFlightType.get());
    }
}
