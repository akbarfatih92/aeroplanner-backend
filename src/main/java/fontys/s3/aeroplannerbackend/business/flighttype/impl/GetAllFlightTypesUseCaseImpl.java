package fontys.s3.aeroplannerbackend.business.flighttype.impl;

import fontys.s3.aeroplannerbackend.business.flighttype.GetAllFlightTypesUseCase;
import fontys.s3.aeroplannerbackend.business.converter.FlightTypeConverter;
import fontys.s3.aeroplannerbackend.domains.flighttype.FlightType;
import fontys.s3.aeroplannerbackend.domains.flighttype.GetAllFlightTypesResponse;
import fontys.s3.aeroplannerbackend.persistence.FlightTypeRepo;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class GetAllFlightTypesUseCaseImpl implements GetAllFlightTypesUseCase {
    private FlightTypeRepo flightTypeRepo;

    @Override
    public GetAllFlightTypesResponse getAllFlightTypes(){
        List<FlightType> result = flightTypeRepo.findAll()
                .stream()
                .map(FlightTypeConverter::convert)
                .toList();

        return GetAllFlightTypesResponse.builder()
                .flightTypes(result)
                .build();
    }
}
