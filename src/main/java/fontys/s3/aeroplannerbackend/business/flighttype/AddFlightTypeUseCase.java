package fontys.s3.aeroplannerbackend.business.flighttype;

import fontys.s3.aeroplannerbackend.domains.GenericIdResponse;
import fontys.s3.aeroplannerbackend.domains.flighttype.AddFlightTypeRequest;

public interface AddFlightTypeUseCase {
    GenericIdResponse addFlightType(AddFlightTypeRequest request);
}
