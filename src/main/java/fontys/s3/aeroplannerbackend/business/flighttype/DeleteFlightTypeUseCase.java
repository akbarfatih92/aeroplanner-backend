package fontys.s3.aeroplannerbackend.business.flighttype;

import fontys.s3.aeroplannerbackend.domains.GenericIdRequest;

public interface DeleteFlightTypeUseCase {
    void deleteFlightType(GenericIdRequest request);
}
