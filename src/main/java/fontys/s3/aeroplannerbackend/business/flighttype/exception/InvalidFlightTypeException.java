package fontys.s3.aeroplannerbackend.business.flighttype.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class InvalidFlightTypeException extends ResponseStatusException {
    public InvalidFlightTypeException(String errorDesc){
        super(HttpStatus.BAD_REQUEST, errorDesc);
    }
}
