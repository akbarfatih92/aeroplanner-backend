package fontys.s3.aeroplannerbackend.business.flighttype;

import fontys.s3.aeroplannerbackend.domains.flighttype.GetAllFlightTypesResponse;

public interface GetAllFlightTypesUseCase {
    GetAllFlightTypesResponse getAllFlightTypes();
}
