package fontys.s3.aeroplannerbackend.business.flighttype.impl;

import fontys.s3.aeroplannerbackend.business.flighttype.AddFlightTypeUseCase;
import fontys.s3.aeroplannerbackend.domains.GenericIdResponse;
import fontys.s3.aeroplannerbackend.domains.flighttype.AddFlightTypeRequest;
import fontys.s3.aeroplannerbackend.persistence.FlightTypeRepo;
import fontys.s3.aeroplannerbackend.persistence.entity.FlightTypeEntity;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class AddFlightTypeUseCaseImpl implements AddFlightTypeUseCase {
    private FlightTypeRepo flightTypeRepo;

    public GenericIdResponse addFlightType(AddFlightTypeRequest request){
        FlightTypeEntity newFlightType = FlightTypeEntity.builder()
                .title(request.getTitle())
                .build();

        FlightTypeEntity responseEntity = flightTypeRepo.save(newFlightType);
        return GenericIdResponse.builder().id(responseEntity.getId()).build();
    }
}
