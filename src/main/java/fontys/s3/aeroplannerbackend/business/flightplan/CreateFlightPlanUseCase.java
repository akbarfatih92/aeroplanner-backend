package fontys.s3.aeroplannerbackend.business.flightplan;

import fontys.s3.aeroplannerbackend.domains.flightplan.CreateFlightPlanRequest;
import fontys.s3.aeroplannerbackend.domains.flightplan.CreateFlightPlanResponse;


public interface CreateFlightPlanUseCase {
    CreateFlightPlanResponse createFlightPlan(CreateFlightPlanRequest request);
}
