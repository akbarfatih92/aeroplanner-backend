package fontys.s3.aeroplannerbackend.business.flightplan.impl;

import fontys.s3.aeroplannerbackend.business.flightplan.GetAllFlightPlansUseCase;
import fontys.s3.aeroplannerbackend.business.converter.FlightPlanConverter;
import fontys.s3.aeroplannerbackend.domains.flightplan.FlightPlan;
import fontys.s3.aeroplannerbackend.domains.flightplan.GetAllFlightPlanRequest;
import fontys.s3.aeroplannerbackend.domains.flightplan.GetAllFlightPlansResponse;
import fontys.s3.aeroplannerbackend.persistence.FlightPlanRepo;
import fontys.s3.aeroplannerbackend.persistence.entity.FlightPlanEntity;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class GetAllFlightPlansUseCaseImpl implements GetAllFlightPlansUseCase {
    private FlightPlanRepo flightPlanRepo;

    @Override
    public GetAllFlightPlansResponse getAllFlightPlans(GetAllFlightPlanRequest request){
        List<FlightPlanEntity> result;

        if(request.getUserId() == null){
            result = flightPlanRepo.findAll();
        }else{
            result = flightPlanRepo.findAllByUserId(request.getUserId());
        }

        final GetAllFlightPlansResponse response = new GetAllFlightPlansResponse();
        List<FlightPlan> flightPlans;
        if(request.getStatus() == null){
             flightPlans = result
                    .stream()
                    .map(FlightPlanConverter::convert)
                    .toList();
        }else{
            flightPlans = result
                    .stream()
                    .filter(fp -> fp.getStatus().equals(request.getStatus()))
                    .map(FlightPlanConverter::convert)
                    .toList();
        }

        response.setFlightPlans(flightPlans);
        return response;
    }
}
