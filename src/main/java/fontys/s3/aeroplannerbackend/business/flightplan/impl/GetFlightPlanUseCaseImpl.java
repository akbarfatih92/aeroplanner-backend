package fontys.s3.aeroplannerbackend.business.flightplan.impl;

import fontys.s3.aeroplannerbackend.business.flightplan.GetFlightPlanUseCase;
import fontys.s3.aeroplannerbackend.business.converter.FlightPlanConverter;
import fontys.s3.aeroplannerbackend.domains.flightplan.FlightPlan;
import fontys.s3.aeroplannerbackend.domains.flightplan.GetFlightPlanRequest;
import fontys.s3.aeroplannerbackend.persistence.FlightPlanRepo;
import fontys.s3.aeroplannerbackend.persistence.entity.FlightPlanEntity;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
public class GetFlightPlanUseCaseImpl implements GetFlightPlanUseCase {
    private FlightPlanRepo flightPlanRepo;

    @Override
    public Optional<FlightPlan> getFlightPlan(GetFlightPlanRequest request){
        Optional<FlightPlanEntity> flightPlanEntity = flightPlanRepo.findByUserIdAndNumber(request.getUserId(), request.getNumber());
        return flightPlanEntity.map(FlightPlanConverter::convert);
    }
}
