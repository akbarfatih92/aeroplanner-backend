package fontys.s3.aeroplannerbackend.business.flightplan;

import fontys.s3.aeroplannerbackend.domains.flightplan.FlightPlan;
import fontys.s3.aeroplannerbackend.domains.flightplan.GetFlightPlanRequest;

import java.util.Optional;

public interface GetFlightPlanUseCase {
    Optional<FlightPlan> getFlightPlan(GetFlightPlanRequest request);
}
