package fontys.s3.aeroplannerbackend.business.flightplan.impl;

import fontys.s3.aeroplannerbackend.business.flightplan.CreateFlightPlanUseCase;
import fontys.s3.aeroplannerbackend.business.aircraft.exception.InvalidAircraftException;
import fontys.s3.aeroplannerbackend.business.airport.exception.InvalidAirportException;
import fontys.s3.aeroplannerbackend.business.flighttype.exception.InvalidFlightTypeException;
import fontys.s3.aeroplannerbackend.business.user.exception.InvalidUserException;
import fontys.s3.aeroplannerbackend.domains.flightplan.CreateFlightPlanRequest;
import fontys.s3.aeroplannerbackend.domains.flightplan.CreateFlightPlanResponse;
import fontys.s3.aeroplannerbackend.persistence.*;
import fontys.s3.aeroplannerbackend.persistence.entity.*;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
public class CreateFlightPlanUseCaseImpl implements CreateFlightPlanUseCase {
    private FlightPlanRepo flightPlanRepo;
    private UserRepo userRepo;
    private AirportRepo airportRepo;
    private AircraftRepo aircraftRepo;
    private FlightTypeRepo flightTypeRepo;

    @Override
    public CreateFlightPlanResponse createFlightPlan(CreateFlightPlanRequest request){
        Optional<UserEntity> selectedUser = userRepo.findById(request.getUserId());
        Optional<AirportEntity> selectedOrigin = airportRepo.findById(request.getOriginId());
        Optional<AirportEntity> selectedDest = airportRepo.findById(request.getDestId());
        Optional<AircraftEntity> selectedAircraft = aircraftRepo.findById(request.getAircraftId());
        Optional<FlightTypeEntity> selectedFlightType = flightTypeRepo.findById(request.getFlightTypeId());
        Long nextNumber = flightPlanRepo.getNextNumber(request.getUserId()).orElse(0L) + 1L;

        String invalidIdMessage = "INVALID ID";
        if(selectedUser.isEmpty()){
            throw new InvalidUserException(invalidIdMessage);
        }
        if(selectedOrigin.isEmpty() || selectedDest.isEmpty()){
            throw new InvalidAirportException(invalidIdMessage);
        }
        if(selectedAircraft.isEmpty()){
            throw new InvalidAircraftException(invalidIdMessage);
        }
        if(selectedFlightType.isEmpty()){
            throw new InvalidFlightTypeException(invalidIdMessage);
        }

        FlightPlanEntity newFlightPlan = FlightPlanEntity.builder()
                .user(selectedUser.get())
                .number(nextNumber)
                .status(request.getStatus())
                .origin(selectedOrigin.get())
                .dest(selectedDest.get())
                .originDateTime(request.getOriginDateTime())
                .destDateTime(request.getDestDateTime())
                .aircraft(selectedAircraft.get())
                .flightType(selectedFlightType.get())
                .flightRule(request.getFlightRule())
                .flightLevel(request.getFlightLevel())
                .cruiseSpeed(request.getCruiseSpeed())
                .flightNumber(request.getFlightNumber())
                .note(request.getNote())
                .build();

        FlightPlanEntity savedFlightPlan = flightPlanRepo.save(newFlightPlan);
        return CreateFlightPlanResponse.builder()
                .userId(savedFlightPlan.getUser().getId())
                .number(savedFlightPlan.getNumber())
                .build();
    }
}
