package fontys.s3.aeroplannerbackend.business.flightplan.impl;

import fontys.s3.aeroplannerbackend.business.flightplan.DeleteFlightPlanUseCase;
import fontys.s3.aeroplannerbackend.business.flightplan.exception.InvalidFlightPlanException;
import fontys.s3.aeroplannerbackend.domains.flightplan.DeleteFlightPlanRequest;
import fontys.s3.aeroplannerbackend.persistence.FlightPlanRepo;
import fontys.s3.aeroplannerbackend.persistence.entity.FlightPlanEntity;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
public class DeleteFlightPlanUseCaseImpl implements DeleteFlightPlanUseCase {
    private final FlightPlanRepo flightPlanRepo;
    @Override
    public void deleteFlightPlan(DeleteFlightPlanRequest request){
        Optional<FlightPlanEntity> fpOptional = flightPlanRepo.findByUserIdAndNumber(request.getUserId(), request.getNumber());
        if(fpOptional.isEmpty()){
            throw new InvalidFlightPlanException("INVALID_USER_ID_OR_NUMBER");
        }
        this.flightPlanRepo.deleteByUserIdAndNumber(request.getUserId(), request.getNumber());
    }
}
