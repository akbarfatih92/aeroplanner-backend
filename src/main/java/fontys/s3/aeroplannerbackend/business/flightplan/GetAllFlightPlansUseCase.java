package fontys.s3.aeroplannerbackend.business.flightplan;

import fontys.s3.aeroplannerbackend.domains.flightplan.GetAllFlightPlanRequest;
import fontys.s3.aeroplannerbackend.domains.flightplan.GetAllFlightPlansResponse;

public interface GetAllFlightPlansUseCase {
    GetAllFlightPlansResponse getAllFlightPlans(GetAllFlightPlanRequest request);
}
