package fontys.s3.aeroplannerbackend.business.flightplan.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class InvalidFlightPlanException extends ResponseStatusException {
    public InvalidFlightPlanException(String errorDesc){
        super(HttpStatus.BAD_REQUEST, errorDesc);
    }
}
