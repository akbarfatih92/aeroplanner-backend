package fontys.s3.aeroplannerbackend.business.flightplan;

import fontys.s3.aeroplannerbackend.domains.flightplan.DeleteFlightPlanRequest;

public interface DeleteFlightPlanUseCase {
    void deleteFlightPlan(DeleteFlightPlanRequest request);
}
