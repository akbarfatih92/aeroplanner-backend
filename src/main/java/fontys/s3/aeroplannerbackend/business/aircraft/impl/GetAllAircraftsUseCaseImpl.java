package fontys.s3.aeroplannerbackend.business.aircraft.impl;

import fontys.s3.aeroplannerbackend.business.aircraft.GetAllAircraftsUseCase;
import fontys.s3.aeroplannerbackend.business.converter.AircraftConverter;
import fontys.s3.aeroplannerbackend.domains.aircraft.Aircraft;
import fontys.s3.aeroplannerbackend.domains.aircraft.GetAllAircraftResponse;
import fontys.s3.aeroplannerbackend.persistence.AircraftRepo;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class GetAllAircraftsUseCaseImpl implements GetAllAircraftsUseCase {
    private AircraftRepo aircraftRepo;

    @Override
    public GetAllAircraftResponse getAllAircrafts(){
        List<Aircraft> result = aircraftRepo.findAll()
                .stream()
                .map(AircraftConverter::convert)
                .toList();

        return GetAllAircraftResponse.builder()
                .aircrafts(result)
                .build();
    }
}
