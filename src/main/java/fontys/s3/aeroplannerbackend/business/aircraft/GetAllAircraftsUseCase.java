package fontys.s3.aeroplannerbackend.business.aircraft;

import fontys.s3.aeroplannerbackend.domains.aircraft.GetAllAircraftResponse;

public interface GetAllAircraftsUseCase {
    GetAllAircraftResponse getAllAircrafts();
}
