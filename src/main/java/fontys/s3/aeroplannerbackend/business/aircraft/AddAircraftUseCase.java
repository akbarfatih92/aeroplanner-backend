package fontys.s3.aeroplannerbackend.business.aircraft;

import fontys.s3.aeroplannerbackend.domains.GenericIdResponse;
import fontys.s3.aeroplannerbackend.domains.aircraft.AddAircraftRequest;

public interface AddAircraftUseCase {
    GenericIdResponse addAircraft(AddAircraftRequest request);
}
