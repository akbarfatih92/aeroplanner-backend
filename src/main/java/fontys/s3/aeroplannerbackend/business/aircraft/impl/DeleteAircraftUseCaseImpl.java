package fontys.s3.aeroplannerbackend.business.aircraft.impl;

import fontys.s3.aeroplannerbackend.business.aircraft.DeleteAircraftUseCase;
import fontys.s3.aeroplannerbackend.business.aircraft.exception.InvalidAircraftException;
import fontys.s3.aeroplannerbackend.domains.GenericIdRequest;
import fontys.s3.aeroplannerbackend.persistence.AircraftRepo;
import fontys.s3.aeroplannerbackend.persistence.entity.AircraftEntity;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
public class DeleteAircraftUseCaseImpl implements DeleteAircraftUseCase {
    private AircraftRepo aircraftRepo;
    @Override
    public void deleteAircraft(GenericIdRequest request){
        Optional<AircraftEntity> selectedAircraft = aircraftRepo.findById(request.getId());

        if(selectedAircraft.isEmpty()){
            throw new InvalidAircraftException("INVALID_AIRCRAFT_ID");
        }

        aircraftRepo.delete(selectedAircraft.get());
    }
}
