package fontys.s3.aeroplannerbackend.business.aircraft;

import fontys.s3.aeroplannerbackend.domains.GenericIdRequest;

public interface DeleteAircraftUseCase {
    void deleteAircraft(GenericIdRequest request);
}
