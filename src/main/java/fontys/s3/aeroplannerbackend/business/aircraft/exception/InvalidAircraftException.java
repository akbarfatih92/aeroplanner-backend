package fontys.s3.aeroplannerbackend.business.aircraft.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class InvalidAircraftException extends ResponseStatusException {
    public InvalidAircraftException(String errorDesc){
        super(HttpStatus.BAD_REQUEST, errorDesc);
    }
}
