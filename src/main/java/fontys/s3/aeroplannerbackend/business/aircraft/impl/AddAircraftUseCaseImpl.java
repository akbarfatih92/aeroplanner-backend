package fontys.s3.aeroplannerbackend.business.aircraft.impl;

import fontys.s3.aeroplannerbackend.business.aircraft.AddAircraftUseCase;
import fontys.s3.aeroplannerbackend.domains.GenericIdResponse;
import fontys.s3.aeroplannerbackend.domains.aircraft.AddAircraftRequest;
import fontys.s3.aeroplannerbackend.persistence.AircraftRepo;
import fontys.s3.aeroplannerbackend.persistence.entity.AircraftEntity;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class AddAircraftUseCaseImpl implements AddAircraftUseCase {
    private AircraftRepo aircraftRepo;

    @Override
    public GenericIdResponse addAircraft(AddAircraftRequest request){
        AircraftEntity newAircraft = AircraftEntity.builder()
                .code(request.getCode())
                .name(request.getName())
                .build();

        AircraftEntity responseEntity = aircraftRepo.save(newAircraft);
        return GenericIdResponse.builder().id(responseEntity.getId()).build();
    }
}
