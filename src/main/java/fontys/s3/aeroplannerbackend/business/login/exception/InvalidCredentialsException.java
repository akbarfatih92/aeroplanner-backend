package fontys.s3.aeroplannerbackend.business.login.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class InvalidCredentialsException extends ResponseStatusException {
    public InvalidCredentialsException(String errorDesc){
        super(HttpStatus.BAD_REQUEST, errorDesc);
    }
}
