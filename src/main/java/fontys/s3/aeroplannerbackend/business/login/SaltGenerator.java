package fontys.s3.aeroplannerbackend.business.login;

public interface SaltGenerator {
    public String generateSalt();
}
