package fontys.s3.aeroplannerbackend.business.login.impl;

import fontys.s3.aeroplannerbackend.business.login.LoginUseCase;
import fontys.s3.aeroplannerbackend.business.login.exception.InvalidCredentialsException;
import fontys.s3.aeroplannerbackend.configuration.security.token.AccessTokenEncoder;
import fontys.s3.aeroplannerbackend.configuration.security.token.impl.AccessTokenImpl;
import fontys.s3.aeroplannerbackend.domains.login.LoginRequest;
import fontys.s3.aeroplannerbackend.domains.login.LoginResponse;
import fontys.s3.aeroplannerbackend.domains.user.UserRole;
import fontys.s3.aeroplannerbackend.persistence.UserRepo;
import fontys.s3.aeroplannerbackend.persistence.entity.UserEntity;
import lombok.AllArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
public class LoginUseCaseImpl implements LoginUseCase {
    private UserRepo userRepo;
    private PasswordEncoder passwordEncoder;
    private AccessTokenEncoder accessTokenEncoder;

    @Override
    public LoginResponse login(LoginRequest loginRequest) {
        Optional<UserEntity> optionalUser = userRepo.findByUsername(loginRequest.getUsername());
        if (optionalUser.isEmpty()) {
            throw new InvalidCredentialsException("INVALID_USERNAME_CREDENTIAL");
        }

        UserEntity user = optionalUser.get();
        if (!matchesPassword(loginRequest.getPassword(), user.getSalt(), user.getHash())) {
            throw new InvalidCredentialsException("INVALID_PASSWORD_CREDENTIAL");
        }

        String accessToken = generateAccessToken(user);
        return LoginResponse.builder().accessToken(accessToken).build();
    }

    private boolean matchesPassword(String rawPassword, String salt, String encodedPassword) {
        String saltedPassword = rawPassword + salt;
        return passwordEncoder.matches(saltedPassword, encodedPassword);
    }

    private String generateAccessToken(UserEntity user) {
        Long id = user.getId();
        UserRole role = UserRole.values()[user.getRole()];

        return accessTokenEncoder.encodeToken(new AccessTokenImpl(user.getUsername(), id, role));
    }
}
