package fontys.s3.aeroplannerbackend.business.login.impl;

import fontys.s3.aeroplannerbackend.business.login.SaltGenerator;
import org.springframework.stereotype.Service;

import java.security.SecureRandom;
import java.util.Base64;

@Service
public class SaltGeneratorImpl implements SaltGenerator {
    @Override
    public String generateSalt() {
        SecureRandom random = new SecureRandom();
        byte[] salt = new byte[5];
        random.nextBytes(salt);
        return Base64.getEncoder().encodeToString(salt);
    }
}
