package fontys.s3.aeroplannerbackend.business.login;

import fontys.s3.aeroplannerbackend.domains.login.LoginRequest;
import fontys.s3.aeroplannerbackend.domains.login.LoginResponse;

public interface LoginUseCase {
    LoginResponse login(LoginRequest request);
}
