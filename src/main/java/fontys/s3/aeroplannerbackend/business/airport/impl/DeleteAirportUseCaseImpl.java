package fontys.s3.aeroplannerbackend.business.airport.impl;

import fontys.s3.aeroplannerbackend.business.airport.DeleteAirportUseCase;
import fontys.s3.aeroplannerbackend.business.airport.exception.InvalidAirportException;
import fontys.s3.aeroplannerbackend.domains.GenericIdRequest;
import fontys.s3.aeroplannerbackend.persistence.AirportRepo;
import fontys.s3.aeroplannerbackend.persistence.entity.AirportEntity;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
public class DeleteAirportUseCaseImpl implements DeleteAirportUseCase {
    private AirportRepo airportRepo;

    @Override
    public void deleteAirport(GenericIdRequest request){
        Optional<AirportEntity> selectedAirport = airportRepo.findById(request.getId());

        if(selectedAirport.isEmpty()){
            throw new InvalidAirportException("INVALID_AIRPORT_ID");
        }

        airportRepo.delete(selectedAirport.get());
    }
}
