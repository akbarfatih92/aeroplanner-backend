package fontys.s3.aeroplannerbackend.business.airport;

import fontys.s3.aeroplannerbackend.domains.GenericIdRequest;

public interface DeleteAirportUseCase {
    void deleteAirport(GenericIdRequest request);
}
