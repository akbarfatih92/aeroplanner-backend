package fontys.s3.aeroplannerbackend.business.airport;

import fontys.s3.aeroplannerbackend.domains.airport.GetAllAirportResponse;

public interface GetAllAirportsUseCase {
    GetAllAirportResponse getAllAirports();
}
