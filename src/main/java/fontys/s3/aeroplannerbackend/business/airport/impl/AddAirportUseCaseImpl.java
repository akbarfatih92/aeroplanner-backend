package fontys.s3.aeroplannerbackend.business.airport.impl;

import fontys.s3.aeroplannerbackend.business.airport.AddAirportUseCase;
import fontys.s3.aeroplannerbackend.domains.GenericIdResponse;
import fontys.s3.aeroplannerbackend.domains.airport.AddAirportRequest;
import fontys.s3.aeroplannerbackend.persistence.AirportRepo;
import fontys.s3.aeroplannerbackend.persistence.entity.AirportEntity;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class AddAirportUseCaseImpl implements AddAirportUseCase {
    private AirportRepo airportRepo;

    @Override
    public GenericIdResponse addAirport(AddAirportRequest request){
        AirportEntity newAirport = AirportEntity.builder()
                .code(request.getCode())
                .name(request.getName())
                .build();

        AirportEntity responseEntity = airportRepo.save(newAirport);
        return GenericIdResponse.builder().id(responseEntity.getId()).build();
    }
}
