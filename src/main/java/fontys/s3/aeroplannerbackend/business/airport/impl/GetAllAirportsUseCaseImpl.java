package fontys.s3.aeroplannerbackend.business.airport.impl;

import fontys.s3.aeroplannerbackend.business.airport.GetAllAirportsUseCase;
import fontys.s3.aeroplannerbackend.business.converter.AirportConverter;
import fontys.s3.aeroplannerbackend.domains.airport.Airport;
import fontys.s3.aeroplannerbackend.domains.airport.GetAllAirportResponse;
import fontys.s3.aeroplannerbackend.persistence.AirportRepo;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class GetAllAirportsUseCaseImpl implements GetAllAirportsUseCase {
    private AirportRepo airportRepo;

    @Override
    public GetAllAirportResponse getAllAirports(){
        List<Airport> result = airportRepo.findAll()
                .stream()
                .map(AirportConverter::convert)
                .toList();

        return GetAllAirportResponse.builder()
                .airports(result)
                .build();
    }
}
