package fontys.s3.aeroplannerbackend.business.airport.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class InvalidAirportException extends ResponseStatusException {
    public InvalidAirportException(String errorDesc){
        super(HttpStatus.BAD_REQUEST, errorDesc);
    }
}
