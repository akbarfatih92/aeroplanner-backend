package fontys.s3.aeroplannerbackend.business.airport;

import fontys.s3.aeroplannerbackend.domains.GenericIdResponse;
import fontys.s3.aeroplannerbackend.domains.airport.AddAirportRequest;


public interface AddAirportUseCase {
    GenericIdResponse addAirport(AddAirportRequest request);
}
