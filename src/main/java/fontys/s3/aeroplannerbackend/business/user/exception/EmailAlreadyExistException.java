package fontys.s3.aeroplannerbackend.business.user.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class EmailAlreadyExistException extends ResponseStatusException {
    public EmailAlreadyExistException(){
        super(HttpStatus.BAD_REQUEST, "EMAIL_ALREADY_EXISTS");
    }
}
