package fontys.s3.aeroplannerbackend.business.user.impl;

import fontys.s3.aeroplannerbackend.business.user.GetAllUsersUseCase;
import fontys.s3.aeroplannerbackend.business.converter.UserConverter;
import fontys.s3.aeroplannerbackend.domains.user.User;
import fontys.s3.aeroplannerbackend.domains.user.GetAllUsersResponse;
import fontys.s3.aeroplannerbackend.persistence.UserRepo;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class GetAllUsersUseCaseImpl implements GetAllUsersUseCase {
    private UserRepo userRepo;

    @Override
    public GetAllUsersResponse getAllUsers(){
        List<User> result = userRepo.findAll()
                .stream()
                .map(UserConverter::convert)
                .toList();

        return GetAllUsersResponse.builder()
                .users(result)
                .build();
    }
}
