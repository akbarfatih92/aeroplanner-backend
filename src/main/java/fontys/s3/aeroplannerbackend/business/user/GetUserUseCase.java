package fontys.s3.aeroplannerbackend.business.user;

import fontys.s3.aeroplannerbackend.domains.GenericIdRequest;
import fontys.s3.aeroplannerbackend.domains.user.User;

import java.util.Optional;

public interface GetUserUseCase {
    Optional<User> getUser(GenericIdRequest request);
}
