package fontys.s3.aeroplannerbackend.business.user;

import fontys.s3.aeroplannerbackend.domains.GenericIdResponse;
import fontys.s3.aeroplannerbackend.domains.user.CreateUserRequest;

public interface CreateUserUseCase {
    GenericIdResponse createUser(CreateUserRequest request);
}
