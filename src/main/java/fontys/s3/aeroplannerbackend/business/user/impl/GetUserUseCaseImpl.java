package fontys.s3.aeroplannerbackend.business.user.impl;

import fontys.s3.aeroplannerbackend.business.converter.UserConverter;
import fontys.s3.aeroplannerbackend.business.user.GetUserUseCase;
import fontys.s3.aeroplannerbackend.business.user.exception.InvalidUserException;
import fontys.s3.aeroplannerbackend.domains.GenericIdRequest;
import fontys.s3.aeroplannerbackend.domains.user.User;
import fontys.s3.aeroplannerbackend.persistence.UserRepo;
import fontys.s3.aeroplannerbackend.persistence.entity.UserEntity;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
public class GetUserUseCaseImpl implements GetUserUseCase {
    private UserRepo userRepo;
    public Optional<User> getUser(GenericIdRequest request){
        Optional<UserEntity> selectedUser = userRepo.findById(request.getId());
        return selectedUser.map(UserConverter::convert);
    }
}
