package fontys.s3.aeroplannerbackend.business.user.impl;

import fontys.s3.aeroplannerbackend.business.user.DeleteUserUseCase;
import fontys.s3.aeroplannerbackend.business.user.exception.InvalidUserException;
import fontys.s3.aeroplannerbackend.domains.GenericIdRequest;
import fontys.s3.aeroplannerbackend.persistence.UserRepo;
import fontys.s3.aeroplannerbackend.persistence.entity.UserEntity;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
public class DeleteUserUseCaseImpl implements DeleteUserUseCase {
    private UserRepo userRepo;
    @Override
    public void deleteUser(GenericIdRequest request){
        Optional<UserEntity> selectedUser = userRepo.findById(request.getId());
        if(selectedUser.isEmpty()){
            throw new InvalidUserException("INVALID_USER_ID");
        }
        userRepo.delete(selectedUser.get());
    }
}
