package fontys.s3.aeroplannerbackend.business.user;

import fontys.s3.aeroplannerbackend.domains.user.GetAllUsersResponse;

public interface GetAllUsersUseCase {
    GetAllUsersResponse getAllUsers();
}
