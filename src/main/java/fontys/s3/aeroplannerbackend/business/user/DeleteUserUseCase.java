package fontys.s3.aeroplannerbackend.business.user;

import fontys.s3.aeroplannerbackend.domains.GenericIdRequest;
import org.springframework.stereotype.Service;

public interface DeleteUserUseCase {
    void deleteUser(GenericIdRequest request);
}
