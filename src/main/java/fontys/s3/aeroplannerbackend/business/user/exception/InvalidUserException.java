package fontys.s3.aeroplannerbackend.business.user.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class InvalidUserException extends ResponseStatusException {
    public InvalidUserException(String errorDesc){
        super(HttpStatus.BAD_REQUEST, errorDesc);
    }
}
