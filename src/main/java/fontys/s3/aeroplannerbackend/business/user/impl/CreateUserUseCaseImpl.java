package fontys.s3.aeroplannerbackend.business.user.impl;

import fontys.s3.aeroplannerbackend.business.login.SaltGenerator;
import fontys.s3.aeroplannerbackend.business.user.CreateUserUseCase;
import fontys.s3.aeroplannerbackend.business.user.exception.EmailAlreadyExistException;
import fontys.s3.aeroplannerbackend.business.user.exception.InvalidUserException;
import fontys.s3.aeroplannerbackend.business.user.exception.UsernameAlreadyExistException;
import fontys.s3.aeroplannerbackend.domains.GenericIdResponse;
import fontys.s3.aeroplannerbackend.domains.user.CreateUserRequest;
import fontys.s3.aeroplannerbackend.persistence.UserRepo;
import fontys.s3.aeroplannerbackend.persistence.entity.UserEntity;
import lombok.AllArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class CreateUserUseCaseImpl implements CreateUserUseCase {
    private UserRepo userRepo;
    private SaltGenerator saltGenerator;
    private PasswordEncoder passwordEncoder;

    @Override
    public GenericIdResponse createUser(CreateUserRequest request){
        String salt = saltGenerator.generateSalt();
        String saltedPassword = request.getPassword() + salt;
        String hash = passwordEncoder.encode(saltedPassword);

        if(userRepo.findByUsername(request.getUsername()).isPresent()){
            System.out.println("USERNAME EXISTED");
            throw new UsernameAlreadyExistException();
        }

        if(userRepo.findByEmail(request.getEmail()).isPresent()){
            System.out.println("EMAIL EXISTED");
            throw new EmailAlreadyExistException();
        }

        UserEntity newUser = UserEntity.builder()
                .username(request.getUsername())
                .email(request.getUsername())
                .hash(hash)
                .salt(salt)
                .role(request.getRole())
                .build();

        UserEntity responseEntity = userRepo.save(newUser);
        return GenericIdResponse.builder().id(responseEntity.getId()).build();
    }
}
