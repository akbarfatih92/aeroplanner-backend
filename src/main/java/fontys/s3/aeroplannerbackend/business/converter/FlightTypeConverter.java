package fontys.s3.aeroplannerbackend.business.converter;

import fontys.s3.aeroplannerbackend.domains.flighttype.FlightType;
import fontys.s3.aeroplannerbackend.persistence.entity.FlightTypeEntity;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access= AccessLevel.PRIVATE)
public class FlightTypeConverter {
    public static FlightType convert(FlightTypeEntity flightType){
        return FlightType.builder()
                .id(flightType.getId())
                .title(flightType.getTitle())
                .build();
    }
}
