package fontys.s3.aeroplannerbackend.business.converter;

import fontys.s3.aeroplannerbackend.domains.aircraft.Aircraft;
import fontys.s3.aeroplannerbackend.persistence.entity.AircraftEntity;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access= AccessLevel.PRIVATE)
public class AircraftConverter {
    public static Aircraft convert(AircraftEntity aircraft){
        return Aircraft.builder()
                .id(aircraft.getId())
                .code(aircraft.getCode())
                .name(aircraft.getName())
                .build();
    }
}
