package fontys.s3.aeroplannerbackend.business.converter;

import fontys.s3.aeroplannerbackend.domains.flightplan.FlightPlan;
import fontys.s3.aeroplannerbackend.domains.user.User;
import fontys.s3.aeroplannerbackend.persistence.entity.FlightPlanEntity;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.time.Duration;

@NoArgsConstructor(access= AccessLevel.PRIVATE)
public class FlightPlanConverter {
    public static FlightPlan convert(FlightPlanEntity flightPlan){
        User convertedUser = UserConverter.convert(flightPlan.getUser());


        return FlightPlan.builder()
                .user(convertedUser)
                .number(flightPlan.getNumber())
                .status(flightPlan.getStatus())
                .origin(AirportConverter.convert(flightPlan.getOrigin()))
                .dest(AirportConverter.convert(flightPlan.getDest()))
                .originDateTime(flightPlan.getOriginDateTime())
                .destDateTime(flightPlan.getDestDateTime())
                .aircraft(AircraftConverter.convert(flightPlan.getAircraft()))
                .flightType(FlightTypeConverter.convert(flightPlan.getFlightType()))
                .flightLevel(flightPlan.getFlightLevel())
                .cruiseSpeed(flightPlan.getCruiseSpeed())
                .flightRule(flightPlan.getFlightRule())
                .flightNumber(flightPlan.getFlightNumber())
                .note(flightPlan.getNote())
                .duration(Duration.between(flightPlan.getOriginDateTime(),flightPlan.getDestDateTime()).toMinutes())
                .build();
    }

}
