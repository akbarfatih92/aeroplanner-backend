package fontys.s3.aeroplannerbackend.business.converter;

import fontys.s3.aeroplannerbackend.domains.airport.Airport;
import fontys.s3.aeroplannerbackend.persistence.entity.AirportEntity;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access= AccessLevel.PRIVATE)
public class AirportConverter {
    public static Airport convert(AirportEntity airport){
        return Airport.builder()
                .id(airport.getId())
                .code(airport.getCode())
                .name(airport.getName())
                .build();
    }
}
