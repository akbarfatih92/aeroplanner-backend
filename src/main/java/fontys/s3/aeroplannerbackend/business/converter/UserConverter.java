package fontys.s3.aeroplannerbackend.business.converter;

import fontys.s3.aeroplannerbackend.domains.user.User;
import fontys.s3.aeroplannerbackend.domains.user.UserRole;
import fontys.s3.aeroplannerbackend.persistence.entity.UserEntity;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access= AccessLevel.PRIVATE)
public class UserConverter {
    public static User convert(UserEntity user){
        return User.builder()
                .id(user.getId())
                .username(user.getUsername())
                .role(UserRole.values()[user.getRole()])
                .createTime(user.getCreateTime())
                .build();
    }
}
