package fontys.s3.aeroplannerbackend.configuration.security.token.impl;


import fontys.s3.aeroplannerbackend.configuration.security.token.AccessToken;
import fontys.s3.aeroplannerbackend.domains.user.UserRole;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class AccessTokenImpl implements AccessToken {
    private final String username;
    private final Long id;
    private final UserRole role;
}
