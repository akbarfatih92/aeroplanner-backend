package fontys.s3.aeroplannerbackend.configuration.security.token;

public interface AccessTokenEncoder {
    String encodeToken(AccessToken accessToken);
}
