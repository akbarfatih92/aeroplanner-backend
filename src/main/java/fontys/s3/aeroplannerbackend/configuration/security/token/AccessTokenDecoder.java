package fontys.s3.aeroplannerbackend.configuration.security.token;

public interface AccessTokenDecoder {
    AccessToken decodeToken(String accessTokenEncoded);
}
