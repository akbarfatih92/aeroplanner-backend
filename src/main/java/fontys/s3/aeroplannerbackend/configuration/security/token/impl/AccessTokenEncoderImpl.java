package fontys.s3.aeroplannerbackend.configuration.security.token.impl;

import fontys.s3.aeroplannerbackend.configuration.security.token.AccessToken;
import fontys.s3.aeroplannerbackend.configuration.security.token.AccessTokenEncoder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.security.Key;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Service
public class AccessTokenEncoderImpl implements AccessTokenEncoder {
    private final Key key;

    public AccessTokenEncoderImpl(@Value("${jwt.secret}") String secretKey) {
        byte[] keyBytes = Decoders.BASE64.decode(secretKey);
        this.key = Keys.hmacShaKeyFor(keyBytes);
    }

    @Override
    public String encodeToken(AccessToken accessToken) {
        Map<String, Object> claimsMap = new HashMap<>();

        claimsMap.put("role", accessToken.getRole().name());
        claimsMap.put("id", accessToken.getId());

        Instant now = Instant.now();
        return Jwts.builder()
                .setSubject(accessToken.getUsername())
                .setIssuedAt(Date.from(now))
                .setExpiration(Date.from(now.plus(30, ChronoUnit.MINUTES)))
                .addClaims(claimsMap)
                .signWith(key)
                .compact();
    }
}
