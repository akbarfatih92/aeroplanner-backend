package fontys.s3.aeroplannerbackend.configuration.security.token.impl;


import fontys.s3.aeroplannerbackend.configuration.security.token.AccessToken;
import fontys.s3.aeroplannerbackend.configuration.security.token.AccessTokenDecoder;
import fontys.s3.aeroplannerbackend.configuration.security.token.exception.InvalidAccessTokenException;
import fontys.s3.aeroplannerbackend.domains.user.UserRole;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwt;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.security.Key;

@Service
public class AccessTokenDecoderImpl implements
        AccessTokenDecoder {
    private final Key key;

    public AccessTokenDecoderImpl(@Value("${jwt.secret}") String secretKey) {
        byte[] keyBytes = Decoders.BASE64.decode(secretKey);
        this.key = Keys.hmacShaKeyFor(keyBytes);
    }

    @Override
    public AccessToken decodeToken(String accessTokenEncoded) {
        try {
            Jwt<?, Claims> jwt = Jwts.parserBuilder().setSigningKey(key).build()
                    .parseClaimsJws(accessTokenEncoded);
            Claims claims = jwt.getBody();

            UserRole role = UserRole.valueOf(claims.get("role", String.class));
            long id = Long.valueOf(claims.get("id", Integer.class));

            return new AccessTokenImpl(claims.getSubject(), id, role);
        } catch (JwtException e) {
            throw new InvalidAccessTokenException(e.getMessage());
        }
    }
}
