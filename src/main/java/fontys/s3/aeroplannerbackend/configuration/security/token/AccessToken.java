package fontys.s3.aeroplannerbackend.configuration.security.token;

import fontys.s3.aeroplannerbackend.domains.user.UserRole;

public interface AccessToken {
    String getUsername();
    Long getId();
    UserRole getRole();
}
