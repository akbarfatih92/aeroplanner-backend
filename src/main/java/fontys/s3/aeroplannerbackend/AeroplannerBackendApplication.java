package fontys.s3.aeroplannerbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AeroplannerBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(AeroplannerBackendApplication.class, args);
    }

}
