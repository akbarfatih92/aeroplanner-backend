//package fontys.s3.aeroplannerbackend.persistence;
//
//
//import fontys.s3.aeroplannerbackend.persistence.entity.UserEntity;
//import jakarta.persistence.EntityManager;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
//
//import static org.junit.jupiter.api.Assertions.*;
//
//@DataJpaTest
//class FlightPlanRepoTest {
//    @Autowired
//    private EntityManager entityManager;
//    @Autowired
//    private FlightPlanRepo flightPlanRepo;
//
//    //private FlightPlanEntity createTestFlightPlan(){}
//
//    private UserEntity createUserIfNeeded(Long userId, String username, String email, String password, int role){
//        return entityManager.createQuery("select f from UserEntity f where f.id = :userId", UserEntity.class)
//                .setParameter("userId",userId)
//                .getResultStream()
//                .findFirst()
//                .orElseGet(() -> entityManager.merge(
//                        UserEntity.builder()
//                                .id(userId)
//                                .username(username)
//                                .email(email)
//                                .password(password)
//                                .role(role)
//                                .build()
//                ));
//    }
//
//    @Test
//    void test(){
//        UserEntity testUser = createUserIfNeeded(1L,"test","test@test.com","test",2);
//        System.out.println(testUser.getUsername());
//    }
//
//}