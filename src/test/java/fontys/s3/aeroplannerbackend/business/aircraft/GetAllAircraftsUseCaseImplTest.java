package fontys.s3.aeroplannerbackend.business.aircraft;

import fontys.s3.aeroplannerbackend.business.aircraft.impl.GetAllAircraftsUseCaseImpl;
import fontys.s3.aeroplannerbackend.business.converter.AircraftConverter;
import fontys.s3.aeroplannerbackend.domains.aircraft.Aircraft;
import fontys.s3.aeroplannerbackend.domains.aircraft.GetAllAircraftResponse;
import fontys.s3.aeroplannerbackend.persistence.AircraftRepo;
import fontys.s3.aeroplannerbackend.persistence.entity.AircraftEntity;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.mockito.Mockito.*;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class GetAllAircraftsUseCaseImplTest {
    @Mock
    AircraftRepo aircraftRepo;

    @InjectMocks
    GetAllAircraftsUseCaseImpl getAllAircraftsUseCase;

    @Test
    void getAllUseCaseAircrafts_returnListOfAll(){
        final AircraftEntity aircraftA = AircraftEntity.builder()
                .id(1L)
                .code("YK42")
                .name("Yakovlev Yak-42")
                .build();

        final AircraftEntity aircraftB = AircraftEntity.builder()
                .id(2L)
                .code("BN2P")
                .name("Pilatus Britten-Norman BN-2A/B Islander")
                .build();

        List<AircraftEntity> entities = List.of(aircraftA,aircraftB);
        List<Aircraft> expectedResult = entities.stream().map(AircraftConverter::convert).toList();

        when(aircraftRepo.findAll()).thenReturn(entities);

        GetAllAircraftResponse actualResult = getAllAircraftsUseCase.getAllAircrafts();
        assertEquals(expectedResult,actualResult.getAircrafts());

        verify(aircraftRepo).findAll();
    }
}