package fontys.s3.aeroplannerbackend.business.flightplan;

import fontys.s3.aeroplannerbackend.business.aircraft.exception.InvalidAircraftException;
import fontys.s3.aeroplannerbackend.business.airport.exception.InvalidAirportException;
import fontys.s3.aeroplannerbackend.business.flighttype.exception.InvalidFlightTypeException;
import fontys.s3.aeroplannerbackend.business.user.exception.InvalidUserException;
import fontys.s3.aeroplannerbackend.business.flightplan.impl.CreateFlightPlanUseCaseImpl;
import fontys.s3.aeroplannerbackend.domains.flightplan.CreateFlightPlanRequest;
import fontys.s3.aeroplannerbackend.domains.flightplan.CreateFlightPlanResponse;
import fontys.s3.aeroplannerbackend.domains.user.User;
import fontys.s3.aeroplannerbackend.domains.user.UserRole;
import fontys.s3.aeroplannerbackend.persistence.*;
import fontys.s3.aeroplannerbackend.persistence.entity.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class CreateFlightPlanUseCaseImplTest {
    @Mock
    UserRepo userRepo;
    @Mock
    AirportRepo airportRepo;
    @Mock
    AircraftRepo aircraftRepo;
    @Mock
    FlightTypeRepo flightTypeRepo;

    @Mock
    FlightPlanRepo flightPlanRepo;
    @InjectMocks
    CreateFlightPlanUseCaseImpl createFlightPlanUseCase;


    final UserEntity userEntity = UserEntity.builder()
            .id(1L)
            .username("john_doe")
            .email("johndoe@mail.com")
            .hash("$2a$12$jPlcJIl/fLAtFv.GMEQXi.O1f.VdKCx5ShmzQGW/9mSwU1U8rYBIu")
            .salt("6BUEM-D#I1n9")
            .role(0)
            .createTime(Timestamp.valueOf(LocalDateTime.now()))
            .build();

    final User user = User.builder()
            .id(userEntity.getId())
            .username(userEntity.getUsername())
            .role(UserRole.PILOT)
            .createTime(userEntity.getCreateTime())
            .build();

    final AirportEntity origin = AirportEntity.builder()
            .id(1L)
            .code("ADMS")
            .name("Admiral Varyag")
            .build();

    final AirportEntity dest = AirportEntity.builder()
            .id(2L)
            .code("AAHA")
            .name("Ahrensdorf Airfield")
            .build();

    final AircraftEntity aircraft = AircraftEntity.builder()
            .id(1L)
            .code("YK42")
            .name("Yakovlev Yak-42")
            .build();

    final FlightTypeEntity flightType = FlightTypeEntity.builder()
            .id(1L)
            .title("Commercial - Scheduled")
            .build();

    final LocalDateTime startTimeTest = LocalDateTime.of(2023, 12 , 31, 12,14);
    final LocalDateTime endTimeTest = LocalDateTime.of(2023, 12 , 31, 15,20);

    @Test
    void createUseCaseFlightPlan_returnIdAndNumber(){
        when(userRepo.findById(1L)).thenReturn(Optional.ofNullable(userEntity));
        when(airportRepo.findById(1L)).thenReturn(Optional.ofNullable(origin));
        when(airportRepo.findById(2L)).thenReturn(Optional.ofNullable(dest));
        when(aircraftRepo.findById(1L)).thenReturn(Optional.ofNullable(aircraft));
        when(flightTypeRepo.findById(1L)).thenReturn(Optional.ofNullable(flightType));

        FlightPlanEntity expectedEntity = FlightPlanEntity.builder()
                .user(userEntity)
                .number(1L)
                .status(1)
                .origin(origin)
                .dest(dest)
                .originDateTime(startTimeTest)
                .destDateTime(endTimeTest)
                .aircraft(aircraft)
                .flightType(flightType)
                .flightRule(1)
                .flightLevel(320)
                .cruiseSpeed(450)
                .flightNumber(123)
                .note("This is a test note for testing stuff.")
                .build();

        when(flightPlanRepo.save(expectedEntity)).thenReturn(expectedEntity);

        CreateFlightPlanRequest request = CreateFlightPlanRequest.builder()
                .userId(1L)
                .status(1)
                .originId(1L)
                .destId(2L)
                .originDateTime(startTimeTest)
                .destDateTime(endTimeTest)
                .aircraftId(1L)
                .flightTypeId(1L)
                .flightRule(1)
                .flightLevel(320)
                .cruiseSpeed(450)
                .flightNumber(123)
                .note("This is a test note for testing stuff.")
                .build();

        CreateFlightPlanResponse response = createFlightPlanUseCase.createFlightPlan(request);
        assertEquals(expectedEntity.getUser().getId(),response.getUserId());
        assertEquals(expectedEntity.getNumber(),response.getNumber());

        verify(userRepo).findById(1L);
        verify(airportRepo).findById(1L);
        verify(airportRepo).findById(2L);
        verify(aircraftRepo).findById(1L);
        verify(flightTypeRepo).findById(1L);
        verify(flightPlanRepo).save(expectedEntity);
    }

    @Test
    void createUseCaseFlightPlan_InvalidUserThrowsIfInvalidId(){
        CreateFlightPlanRequest request = CreateFlightPlanRequest.builder()
                .userId(2L)
                .status(1)
                .originId(1L)
                .destId(2L)
                .originDateTime(startTimeTest)
                .destDateTime(endTimeTest)
                .aircraftId(1L)
                .flightTypeId(1L)
                .flightRule(1)
                .flightLevel(320)
                .cruiseSpeed(450)
                .flightNumber(123)
                .note("This is a test note for testing stuff.")
                .build();


        assertThrows(InvalidUserException.class,()->{
            CreateFlightPlanResponse response = createFlightPlanUseCase.createFlightPlan(request);
        });

        verify(userRepo).findById(2L);
    }

    @Test
    void createUseCaseFlightPlan_InvalidAirportThrowsIfInvalidId(){
        when(userRepo.findById(1L)).thenReturn(Optional.ofNullable(userEntity));

        CreateFlightPlanRequest requestOrigin = CreateFlightPlanRequest.builder()
                .userId(1L)
                .status(1)
                .originId(3L)
                .destId(2L)
                .originDateTime(startTimeTest)
                .destDateTime(endTimeTest)
                .aircraftId(1L)
                .flightTypeId(1L)
                .flightRule(1)
                .flightLevel(320)
                .cruiseSpeed(450)
                .flightNumber(123)
                .note("This is a test note for testing stuff.")
                .build();


        assertThrows(InvalidAirportException.class,()->{
            CreateFlightPlanResponse response = createFlightPlanUseCase.createFlightPlan(requestOrigin);
        });

        verify(airportRepo).findById(3L);

        CreateFlightPlanRequest requestDest = CreateFlightPlanRequest.builder()
                .userId(1L)
                .status(1)
                .originId(1L)
                .destId(4L)
                .originDateTime(startTimeTest)
                .destDateTime(endTimeTest)
                .aircraftId(1L)
                .flightTypeId(1L)
                .flightRule(1)
                .flightLevel(320)
                .cruiseSpeed(450)
                .flightNumber(123)
                .note("This is a test note for testing stuff.")
                .build();


        assertThrows(InvalidAirportException.class,()->{
            CreateFlightPlanResponse response = createFlightPlanUseCase.createFlightPlan(requestDest);
        });

        verify(airportRepo).findById(4L);
    }

    @Test
    void createUseCaseFlightPlan_InvalidAircraftThrowsIfInvalidId(){
        when(userRepo.findById(1L)).thenReturn(Optional.ofNullable(userEntity));
        when(airportRepo.findById(1L)).thenReturn(Optional.ofNullable(origin));
        when(airportRepo.findById(2L)).thenReturn(Optional.ofNullable(dest));
        CreateFlightPlanRequest request = CreateFlightPlanRequest.builder()
                .userId(1L)
                .status(1)
                .originId(1L)
                .destId(2L)
                .originDateTime(startTimeTest)
                .destDateTime(endTimeTest)
                .aircraftId(2L)
                .flightTypeId(1L)
                .flightRule(1)
                .flightLevel(320)
                .cruiseSpeed(450)
                .flightNumber(123)
                .note("This is a test note for testing stuff.")
                .build();


        assertThrows(InvalidAircraftException.class,()->{
            CreateFlightPlanResponse response = createFlightPlanUseCase.createFlightPlan(request);
        });

        verify(aircraftRepo).findById(2L);
    }

    @Test
    void createUseCaseFlightPlan_InvalidFlightTypeThrowsIfInvalidId(){
        when(userRepo.findById(1L)).thenReturn(Optional.ofNullable(userEntity));
        when(airportRepo.findById(1L)).thenReturn(Optional.ofNullable(origin));
        when(airportRepo.findById(2L)).thenReturn(Optional.ofNullable(dest));
        when(aircraftRepo.findById(1L)).thenReturn(Optional.ofNullable(aircraft));
        CreateFlightPlanRequest request = CreateFlightPlanRequest.builder()
                .userId(1L)
                .status(1)
                .originId(1L)
                .destId(2L)
                .originDateTime(startTimeTest)
                .destDateTime(endTimeTest)
                .aircraftId(1L)
                .flightTypeId(2L)
                .flightRule(1)
                .flightLevel(320)
                .cruiseSpeed(450)
                .flightNumber(123)
                .note("This is a test note for testing stuff.")
                .build();


        assertThrows(InvalidFlightTypeException.class,()->{
            CreateFlightPlanResponse response = createFlightPlanUseCase.createFlightPlan(request);
        });

        verify(flightTypeRepo).findById(2L);
    }

}