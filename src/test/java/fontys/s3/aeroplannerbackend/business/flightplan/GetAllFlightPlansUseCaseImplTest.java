package fontys.s3.aeroplannerbackend.business.flightplan;

import fontys.s3.aeroplannerbackend.business.converter.FlightPlanConverter;
import fontys.s3.aeroplannerbackend.business.flightplan.impl.GetAllFlightPlansUseCaseImpl;
import fontys.s3.aeroplannerbackend.domains.flightplan.FlightPlan;
import fontys.s3.aeroplannerbackend.domains.flightplan.GetAllFlightPlanRequest;
import fontys.s3.aeroplannerbackend.domains.flightplan.GetAllFlightPlansResponse;
import fontys.s3.aeroplannerbackend.persistence.FlightPlanRepo;
import fontys.s3.aeroplannerbackend.persistence.entity.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

import static org.mockito.Mockito.*;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class GetAllFlightPlansUseCaseImplTest {
    @Mock
    FlightPlanRepo flightPlanRepo;

    @InjectMocks
    GetAllFlightPlansUseCaseImpl getAllFlightPlansUseCase;

    final UserEntity userA = UserEntity.builder()
            .id(1L)
            .username("john_doe")
            .email("johndoe@mail.com")
            .hash("$2a$12$jPlcJIl/fLAtFv.GMEQXi.O1f.VdKCx5ShmzQGW/9mSwU1U8rYBIu")
            .salt("6BUEM-D#I1n9")
            .role(0)
            .createTime(Timestamp.valueOf(LocalDateTime.now()))
            .build();

    final UserEntity userB = UserEntity.builder()
            .id(2L)
            .username("jane_doe")
            .email("janedoe@mail.com")
            .hash("$2a$12$QcPcom7t0JrBKjT3hxyxheKhWXYhw3kitvfL2LMy.BiBniBavcyFe")
            .salt("K4X@7tcOA=ar")
            .role(0)
            .createTime(Timestamp.valueOf(LocalDateTime.now()))
            .build();

    final AirportEntity origin = AirportEntity.builder()
            .id(1L)
            .code("ADMS")
            .name("Admiral Varyag")
            .build();

    final AirportEntity dest = AirportEntity.builder()
            .id(2L)
            .code("AAHA")
            .name("Ahrensdorf Airfield")
            .build();

    final AircraftEntity aircraft = AircraftEntity.builder()
            .id(1L)
            .code("YK42")
            .name("Yakovlev Yak-42")
            .build();

    final FlightTypeEntity flightType = FlightTypeEntity.builder()
            .id(1L)
            .title("Commercial - Scheduled")
            .build();

    final LocalDateTime startTimeTest = LocalDateTime.of(2023, 12 , 31, 12,14);
    final LocalDateTime endTimeTest = LocalDateTime.of(2023, 12 , 31, 15,20);


    final FlightPlanEntity flightPlanEntityA = FlightPlanEntity.builder()
            .user(userA)
            .number(1L)
            .status(1)
            .origin(origin)
            .dest(dest)
            .originDateTime(startTimeTest)
            .destDateTime(endTimeTest)
            .aircraft(aircraft)
            .flightType(flightType)
            .flightRule(1)
            .flightLevel(320)
            .cruiseSpeed(450)
            .flightNumber(123)
            .note("This is a test note for testing stuff.")
            .build();

    final FlightPlanEntity flightPlanEntityB = FlightPlanEntity.builder()
            .user(userB)
            .number(1L)
            .status(1)
            .origin(origin)
            .dest(dest)
            .originDateTime(startTimeTest)
            .destDateTime(endTimeTest)
            .aircraft(aircraft)
            .flightType(flightType)
            .flightRule(1)
            .flightLevel(300)
            .cruiseSpeed(400)
            .build();

    final FlightPlanEntity flightPlanEntityC = FlightPlanEntity.builder()
            .user(userA)
            .number(2L)
            .status(2)
            .origin(origin)
            .dest(dest)
            .originDateTime(startTimeTest)
            .destDateTime(endTimeTest)
            .aircraft(aircraft)
            .flightType(flightType)
            .flightRule(2)
            .flightLevel(200)
            .cruiseSpeed(455)
            .flightNumber(10)
            .note("Stuff.")
            .build();

    @Test
    void getAllUseCaseFlightPlans_returnListOfAll(){
        List<FlightPlanEntity> entitiesList = List.of(flightPlanEntityA,flightPlanEntityB,flightPlanEntityC);
        List<FlightPlan> expectedResult = entitiesList.stream().map(FlightPlanConverter::convert).toList();

        when(flightPlanRepo.findAll()).thenReturn(entitiesList);

        GetAllFlightPlanRequest request = GetAllFlightPlanRequest.builder().build();
        GetAllFlightPlansResponse response = getAllFlightPlansUseCase.getAllFlightPlans(request);
        assertEquals(expectedResult,response.getFlightPlans());

        verify(flightPlanRepo).findAll();
    }

    @Test
    void getAllUseCaseFlightPlans_returnAListOfSpecificStatusOnly(){
        List<FlightPlanEntity> entitiesList = List.of(flightPlanEntityA,flightPlanEntityB,flightPlanEntityC);
        List<FlightPlan> expectedResult = entitiesList.stream().filter(fp -> fp.getStatus().equals(1)).map(FlightPlanConverter::convert).toList();

        when(flightPlanRepo.findAll()).thenReturn(entitiesList);

        GetAllFlightPlanRequest request = GetAllFlightPlanRequest.builder().status(1).build();
        GetAllFlightPlansResponse response = getAllFlightPlansUseCase.getAllFlightPlans(request);
        assertEquals(expectedResult,response.getFlightPlans());

        verify(flightPlanRepo).findAll();
    }

    @Test
    void getAllUseCaseFlightPlans_returnAListOfSpecificUserOnly(){
        List<FlightPlanEntity> entitiesList = List.of(flightPlanEntityA,flightPlanEntityC);
        List<FlightPlan> expectedResult = entitiesList.stream().map(FlightPlanConverter::convert).toList();

        when(flightPlanRepo.findAllByUserId(1L)).thenReturn(entitiesList);

        GetAllFlightPlanRequest request = GetAllFlightPlanRequest.builder().userId(1L).build();
        GetAllFlightPlansResponse response = getAllFlightPlansUseCase.getAllFlightPlans(request);
        assertEquals(expectedResult,response.getFlightPlans());

        verify(flightPlanRepo).findAllByUserId(1L);
    }

    @Test
    void getAllUseCaseFlightPlans_returnAListOfSpecificUserAndStatusOnly(){
        List<FlightPlanEntity> entitiesList = List.of(flightPlanEntityA,flightPlanEntityC);
        List<FlightPlan> expectedResult = entitiesList.stream().filter(fp -> fp.getStatus().equals(1)).map(FlightPlanConverter::convert).toList();

        when(flightPlanRepo.findAllByUserId(1L)).thenReturn(entitiesList);

        GetAllFlightPlanRequest request = GetAllFlightPlanRequest.builder().status(1).userId(1L).build();
        GetAllFlightPlansResponse response = getAllFlightPlansUseCase.getAllFlightPlans(request);
        assertEquals(expectedResult,response.getFlightPlans());

        verify(flightPlanRepo).findAllByUserId(1L);
    }
}