package fontys.s3.aeroplannerbackend.business.flightplan;

import fontys.s3.aeroplannerbackend.business.flightplan.exception.InvalidFlightPlanException;
import fontys.s3.aeroplannerbackend.business.flightplan.impl.DeleteFlightPlanUseCaseImpl;
import fontys.s3.aeroplannerbackend.domains.flightplan.DeleteFlightPlanRequest;
import fontys.s3.aeroplannerbackend.persistence.FlightPlanRepo;
import fontys.s3.aeroplannerbackend.persistence.entity.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
class DeleteFlightPlanUseCaseImplTest {
    @Mock
    FlightPlanRepo flightPlanRepo;
    @InjectMocks
    DeleteFlightPlanUseCaseImpl deleteFlightPlanUseCase;

    final UserEntity userEntity = UserEntity.builder()
            .id(1L)
            .username("john_doe")
            .email("johndoe@mail.com")
            .hash("$2a$12$jPlcJIl/fLAtFv.GMEQXi.O1f.VdKCx5ShmzQGW/9mSwU1U8rYBIu")
            .salt("6BUEM-D#I1n9")
            .role(0)
            .createTime(Timestamp.valueOf(LocalDateTime.now()))
            .build();

    final AirportEntity origin = AirportEntity.builder()
            .id(1L)
            .code("ADMS")
            .name("Admiral Varyag")
            .build();

    final AirportEntity dest = AirportEntity.builder()
            .id(2L)
            .code("AAHA")
            .name("Ahrensdorf Airfield")
            .build();

    final AircraftEntity aircraft = AircraftEntity.builder()
            .id(1L)
            .code("YK42")
            .name("Yakovlev Yak-42")
            .build();

    final FlightTypeEntity flightType = FlightTypeEntity.builder()
            .id(1L)
            .title("Commercial - Scheduled")
            .build();

    final LocalDateTime startTimeTest = LocalDateTime.of(2023, 12 , 31, 12,14);
    final LocalDateTime endTimeTest = LocalDateTime.of(2023, 12 , 31, 15,20);

    @Test
    void deleteUseCaseFlightPlan_checkDeleteFunctionAreCalled(){
        FlightPlanEntity flightPlanEntity = FlightPlanEntity.builder()
                .user(userEntity)
                .number(1L)
                .status(1)
                .origin(origin)
                .dest(dest)
                .originDateTime(startTimeTest)
                .destDateTime(endTimeTest)
                .aircraft(aircraft)
                .flightType(flightType)
                .flightRule(1)
                .flightLevel(320)
                .cruiseSpeed(450)
                .flightNumber(123)
                .note("This is a test note for testing stuff.")
                .build();

        when(flightPlanRepo.findByUserIdAndNumber(1L,1L)).thenReturn(Optional.ofNullable(flightPlanEntity));
        doNothing().when(flightPlanRepo).deleteByUserIdAndNumber(1L,1L);

        DeleteFlightPlanRequest request = DeleteFlightPlanRequest.builder().userId(1L).number(1L).build();
        this.deleteFlightPlanUseCase.deleteFlightPlan(request);

        verify(flightPlanRepo).findByUserIdAndNumber(1L,1L);
        verify(flightPlanRepo).deleteByUserIdAndNumber(1L,1L);
    }

    @Test
    void deleteUseCaseFlightPlan_throwExceptionIfInvalidID(){
        when(flightPlanRepo.findByUserIdAndNumber(2L,2L)).thenReturn(Optional.empty());

        DeleteFlightPlanRequest request = DeleteFlightPlanRequest.builder().userId(2L).number(2L).build();

        assertThrows(InvalidFlightPlanException.class,() -> {
            this.deleteFlightPlanUseCase.deleteFlightPlan(request);
        });

        verify(flightPlanRepo).findByUserIdAndNumber(2L,2L);
        verify(flightPlanRepo, never()).deleteByUserIdAndNumber(2L,2L);
    }
}