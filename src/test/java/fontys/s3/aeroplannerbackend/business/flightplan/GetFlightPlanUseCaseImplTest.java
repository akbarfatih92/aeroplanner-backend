package fontys.s3.aeroplannerbackend.business.flightplan;

import fontys.s3.aeroplannerbackend.business.converter.AircraftConverter;
import fontys.s3.aeroplannerbackend.business.converter.AirportConverter;
import fontys.s3.aeroplannerbackend.business.converter.FlightTypeConverter;
import fontys.s3.aeroplannerbackend.business.flightplan.impl.GetFlightPlanUseCaseImpl;
import fontys.s3.aeroplannerbackend.domains.flightplan.FlightPlan;
import fontys.s3.aeroplannerbackend.domains.flightplan.GetFlightPlanRequest;
import fontys.s3.aeroplannerbackend.domains.user.User;
import fontys.s3.aeroplannerbackend.domains.user.UserRole;
import fontys.s3.aeroplannerbackend.persistence.FlightPlanRepo;
import fontys.s3.aeroplannerbackend.persistence.entity.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.Timestamp;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class GetFlightPlanUseCaseImplTest {
    @Mock
    FlightPlanRepo flightPlanRepo;
    @InjectMocks
    GetFlightPlanUseCaseImpl getFlightPlanUseCase;

    final UserEntity userEntity = UserEntity.builder()
            .id(1L)
            .username("john_doe")
            .email("johndoe@mail.com")
            .hash("$2a$12$jPlcJIl/fLAtFv.GMEQXi.O1f.VdKCx5ShmzQGW/9mSwU1U8rYBIu")
            .salt("6BUEM-D#I1n9")
            .role(0)
            .createTime(Timestamp.valueOf(LocalDateTime.now()))
            .build();

    final User user = User.builder()
            .id(userEntity.getId())
            .username(userEntity.getUsername())
            .role(UserRole.PILOT)
            .createTime(userEntity.getCreateTime())
            .build();

    final AirportEntity origin = AirportEntity.builder()
            .id(1L)
            .code("ADMS")
            .name("Admiral Varyag")
            .build();

    final AirportEntity dest = AirportEntity.builder()
            .id(2L)
            .code("AAHA")
            .name("Ahrensdorf Airfield")
            .build();

    final AircraftEntity aircraft = AircraftEntity.builder()
            .id(1L)
            .code("YK42")
            .name("Yakovlev Yak-42")
            .build();

    final FlightTypeEntity flightType = FlightTypeEntity.builder()
            .id(1L)
            .title("Commercial - Scheduled")
            .build();

    final LocalDateTime startTimeTest = LocalDateTime.of(2023, 12 , 31, 12,14);
    final LocalDateTime endTimeTest = LocalDateTime.of(2023, 12 , 31, 15,20);

    @Test
    void getUseCaseFlightPlan_returnFlightPlan(){
        FlightPlanEntity flightPlanEntity = FlightPlanEntity.builder()
                .user(userEntity)
                .number(1L)
                .status(1)
                .origin(origin)
                .dest(dest)
                .originDateTime(startTimeTest)
                .destDateTime(endTimeTest)
                .aircraft(aircraft)
                .flightType(flightType)
                .flightRule(1)
                .flightLevel(320)
                .cruiseSpeed(450)
                .flightNumber(123)
                .note("This is a test note for testing stuff.")
                .build();

        when(flightPlanRepo.findByUserIdAndNumber(1L,1L)).thenReturn(Optional.ofNullable(flightPlanEntity));

        FlightPlan expectedFlightPlan = FlightPlan.builder()
                .user(user)
                .number(1L)
                .status(1)
                .origin(AirportConverter.convert(origin))
                .dest(AirportConverter.convert(dest))
                .originDateTime(startTimeTest)
                .destDateTime(endTimeTest)
                .aircraft(AircraftConverter.convert(aircraft))
                .flightType(FlightTypeConverter.convert(flightType))
                .flightRule(1)
                .flightLevel(320)
                .cruiseSpeed(450)
                .flightNumber(123)
                .note("This is a test note for testing stuff.")
                .duration(Duration.between(startTimeTest,endTimeTest).toMinutes())
                .build();

        GetFlightPlanRequest request = GetFlightPlanRequest.builder().userId(1L).number(1L).build();
        Optional<FlightPlan> actualFlightPlan = getFlightPlanUseCase.getFlightPlan(request);

        assertEquals(expectedFlightPlan,actualFlightPlan.get());

        verify(flightPlanRepo).findByUserIdAndNumber(1L,1L);
    }

    @Test
    void getUseCaseFlightPlan_returnEmptyOptionalIfEmpty(){
        GetFlightPlanRequest request = GetFlightPlanRequest.builder().userId(2L).number(2L).build();
        Optional<FlightPlan> actualFlightPlan = getFlightPlanUseCase.getFlightPlan(request);

        assertEquals(Optional.empty(),actualFlightPlan);

        verify(flightPlanRepo).findByUserIdAndNumber(2L,2L);
    }
}