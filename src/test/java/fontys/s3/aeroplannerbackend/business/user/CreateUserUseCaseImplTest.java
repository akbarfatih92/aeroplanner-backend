package fontys.s3.aeroplannerbackend.business.user;

import fontys.s3.aeroplannerbackend.business.login.SaltGenerator;
import fontys.s3.aeroplannerbackend.business.user.impl.CreateUserUseCaseImpl;
import fontys.s3.aeroplannerbackend.domains.GenericIdResponse;
import fontys.s3.aeroplannerbackend.domains.user.CreateUserRequest;
import fontys.s3.aeroplannerbackend.persistence.UserRepo;
import fontys.s3.aeroplannerbackend.persistence.entity.UserEntity;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class CreateUserUseCaseImplTest {
    @Mock
    UserRepo userRepo;

    @Mock
    SaltGenerator saltGenerator;

    @Mock
    PasswordEncoder passwordEncoder;

    @InjectMocks
    CreateUserUseCaseImpl createUserUseCase;

    @Test
    void createUseCaseUser_returnId(){
        CreateUserRequest request = CreateUserRequest.builder()
                .username("john_doe")
                .email("johndoe@mail.com")
                .password("12345StrongPassword")
                .role(0)
                .build();

        UserEntity userEntity = UserEntity.builder()
                .id(1L)
                .username("john_doe")
                .email("johndoe@mail.com")
                .hash("$2a$12$jPlcJIl/fLAtFv.GMEQXi.O1f.VdKCx5ShmzQGW/9mSwU1U8rYBIu")
                .salt("6BUEM-D#I1n9")
                .role(0)
                .createTime(Timestamp.valueOf(LocalDateTime.now()))
                .build();

        when(saltGenerator.generateSalt()).thenReturn(userEntity.getSalt());
        when(passwordEncoder.encode(anyString())).thenReturn(userEntity.getHash());

        when(userRepo.findByUsername(request.getUsername())).thenReturn(Optional.empty());
        when(userRepo.findByEmail(request.getEmail())).thenReturn(Optional.empty());
        when(userRepo.save(any(UserEntity.class))).thenReturn(userEntity);

        GenericIdResponse response = createUserUseCase.createUser(request);

        assertEquals(userEntity.getId(),response.getId());

        verify(saltGenerator).generateSalt();
        verify(passwordEncoder).encode(anyString());

        verify(userRepo).findByUsername(request.getUsername());
        verify(userRepo).findByEmail(request.getEmail());
        verify(userRepo).save(any(UserEntity.class));
    }
}