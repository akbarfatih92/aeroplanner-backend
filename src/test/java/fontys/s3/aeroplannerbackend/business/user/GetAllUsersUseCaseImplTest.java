package fontys.s3.aeroplannerbackend.business.user;

import fontys.s3.aeroplannerbackend.business.converter.UserConverter;
import fontys.s3.aeroplannerbackend.business.user.impl.GetAllUsersUseCaseImpl;
import fontys.s3.aeroplannerbackend.domains.user.User;
import fontys.s3.aeroplannerbackend.domains.user.GetAllUsersResponse;
import fontys.s3.aeroplannerbackend.persistence.UserRepo;
import fontys.s3.aeroplannerbackend.persistence.entity.UserEntity;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class GetAllUsersUseCaseImplTest {
    @Mock
    UserRepo userRepo;

    @InjectMocks
    GetAllUsersUseCaseImpl getAllUsersUseCase;

    @Test
    void getAllUseCaseUsers_returnListOfAll(){
        final UserEntity userA = UserEntity.builder()
                .id(1L)
                .username("john_doe")
                .email("johndoe@mail.com")
                .hash("$2a$12$jPlcJIl/fLAtFv.GMEQXi.O1f.VdKCx5ShmzQGW/9mSwU1U8rYBIu")
                .salt("6BUEM-D#I1n9")
                .role(0)
                .createTime(Timestamp.valueOf(LocalDateTime.now()))
                .build();

        final UserEntity userB = UserEntity.builder()
                .id(2L)
                .username("jane_doe")
                .email("janedoe@mail.com")
                .hash("$2a$12$QcPcom7t0JrBKjT3hxyxheKhWXYhw3kitvfL2LMy.BiBniBavcyFe")
                .salt("K4X@7tcOA=ar")
                .role(0)
                .createTime(Timestamp.valueOf(LocalDateTime.now()))
                .build();

        List<UserEntity> entities = List.of(userA,userB);
        List<User> expectedResult = entities.stream().map(UserConverter::convert).toList();

        when(userRepo.findAll()).thenReturn(entities);

        GetAllUsersResponse actualResult = getAllUsersUseCase.getAllUsers();
        assertEquals(expectedResult,actualResult.getUsers());

        verify(userRepo).findAll();
    }
}