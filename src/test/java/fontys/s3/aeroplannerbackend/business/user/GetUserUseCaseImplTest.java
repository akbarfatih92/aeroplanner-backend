package fontys.s3.aeroplannerbackend.business.user;

import fontys.s3.aeroplannerbackend.business.user.impl.GetUserUseCaseImpl;
import fontys.s3.aeroplannerbackend.domains.GenericIdRequest;
import fontys.s3.aeroplannerbackend.domains.user.User;
import fontys.s3.aeroplannerbackend.domains.user.UserRole;
import fontys.s3.aeroplannerbackend.persistence.UserRepo;
import fontys.s3.aeroplannerbackend.persistence.entity.UserEntity;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class GetUserUseCaseImplTest {
    @Mock
    UserRepo userRepo;

    @InjectMocks
    GetUserUseCaseImpl getUserUseCase;

    final UserEntity userEntity = UserEntity.builder()
            .id(1L)
            .username("john_doe")
            .email("johndoe@mail.com")
            .hash("$2a$12$jPlcJIl/fLAtFv.GMEQXi.O1f.VdKCx5ShmzQGW/9mSwU1U8rYBIu")
            .salt("6BUEM-D#I1n9")
            .role(0)
            .createTime(Timestamp.valueOf(LocalDateTime.now()))
            .build();

    @Test
    void getUseCaseUser_returnUser(){
        when(userRepo.findById(1L)).thenReturn(Optional.of(userEntity));
        User expectedUser = User.builder()
                .id(userEntity.getId())
                .username(userEntity.getUsername())
                .role(UserRole.PILOT)
                .createTime(userEntity.getCreateTime())
                .build();

        GenericIdRequest request = GenericIdRequest.builder().id(1L).build();
        Optional<User> actualUser = getUserUseCase.getUser(request);

        assertEquals(expectedUser, actualUser.get());

        verify(userRepo).findById(1L);
    }

    @Test
    void getUseCaseUser_returnEmptyOptionalIfEmpty(){
        when(userRepo.findById(2L)).thenReturn(Optional.empty());
        GenericIdRequest request = GenericIdRequest.builder().id(2L).build();
        Optional<User> actualUser = getUserUseCase.getUser(request);

        assertEquals(Optional.empty(), actualUser);

        verify(userRepo).findById(2L);
    }
}