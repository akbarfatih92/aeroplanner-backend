package fontys.s3.aeroplannerbackend.business.flighttype;

import fontys.s3.aeroplannerbackend.business.converter.FlightTypeConverter;
import fontys.s3.aeroplannerbackend.business.flighttype.impl.GetAllFlightTypesUseCaseImpl;
import fontys.s3.aeroplannerbackend.domains.flighttype.FlightType;
import fontys.s3.aeroplannerbackend.domains.flighttype.GetAllFlightTypesResponse;
import fontys.s3.aeroplannerbackend.persistence.FlightTypeRepo;
import fontys.s3.aeroplannerbackend.persistence.entity.FlightTypeEntity;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.mockito.Mockito.*;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class GetAllFlightTypesUseCaseImplTest {
    @Mock
    FlightTypeRepo flightTypeRepo;

    @InjectMocks
    GetAllFlightTypesUseCaseImpl getAllFlightTypesUseCase;

    @Test
    void getAllUseCaseFlightTypes_returnListOfAll(){
        FlightTypeEntity flightTypeA = FlightTypeEntity.builder()
                .id(1L)
                .title("Commercial - Scheduled")
                .build();

        FlightTypeEntity flightTypeB = FlightTypeEntity.builder()
                .id(2L)
                .title("Commercial - Charter")
                .build();

        List<FlightTypeEntity> entities = List.of(flightTypeA,flightTypeB);
        List<FlightType> expectedResult = entities.stream().map(FlightTypeConverter::convert).toList();

        when(flightTypeRepo.findAll()).thenReturn(entities);

        GetAllFlightTypesResponse actualResult = getAllFlightTypesUseCase.getAllFlightTypes();
        assertEquals(expectedResult,actualResult.getFlightTypes());

        verify(flightTypeRepo).findAll();
    }
}