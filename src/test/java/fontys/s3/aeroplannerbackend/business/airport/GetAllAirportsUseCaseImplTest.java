package fontys.s3.aeroplannerbackend.business.airport;

import fontys.s3.aeroplannerbackend.business.airport.impl.GetAllAirportsUseCaseImpl;
import fontys.s3.aeroplannerbackend.business.converter.AirportConverter;
import fontys.s3.aeroplannerbackend.domains.airport.Airport;
import fontys.s3.aeroplannerbackend.domains.airport.GetAllAirportResponse;
import fontys.s3.aeroplannerbackend.persistence.AirportRepo;
import fontys.s3.aeroplannerbackend.persistence.entity.AirportEntity;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.mockito.Mockito.*;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class GetAllAirportsUseCaseImplTest {
    @Mock
    AirportRepo airportRepo;

    @InjectMocks
    GetAllAirportsUseCaseImpl getAllAirportsUseCase;

    @Test
    void getAllUseCaseAirports_returnListOfAll(){
        final AirportEntity airportA = AirportEntity.builder()
                .id(1L)
                .code("ADMS")
                .name("Admiral Varyag")
                .build();

        final AirportEntity airportB = AirportEntity.builder()
                .id(2L)
                .code("AAHA")
                .name("Ahrensdorf Airfield")
                .build();

        List<AirportEntity> entities = List.of(airportA,airportB);
        List<Airport> expectedResult = entities.stream().map(AirportConverter::convert).toList();

        when(airportRepo.findAll()).thenReturn(entities);

        GetAllAirportResponse actualResult = getAllAirportsUseCase.getAllAirports();
        assertEquals(expectedResult,actualResult.getAirports());

        verify(airportRepo).findAll();
    }
}